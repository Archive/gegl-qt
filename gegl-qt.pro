include(config.pri)
isEmpty(GEGLQT_CONFIG):error("config.pri not found")

TEMPLATE = subdirs
CONFIG += ordered

# Subdirectories
isEmpty(USE_EXTERNAL_GEGLQT) {
    SUBDIRS += gegl-qt
    contains(HAVE_PYSIDE, yes) {
        SUBDIRS += pygegl-qt
    }
    contains(HAVE_QT_WIDGETS, yes) {
        SUBDIRS += operations
    }
    SUBDIRS +=  plugins doc

} else {
    !system(pkg-config --exists $$GEGLQT_LIBNAME):error("Could not find required dependency: GEGL-QT")
}
SUBDIRS += examples tests

### Extra targets for distribution ###

DIST_NAME = $$GEGLQT_PROJECTNAME-$$GEGLQT_VERSION
DIST_PATH = $$OUT_PWD/$$DIST_NAME
TARBALL_SUFFIX = .tar.bz2
TARBALL_PATH = $$DIST_PATH$$TARBALL_SUFFIX
CHECK_INSTALL_ROOT = $$OUT_PWD/distcheck-install
CHECK_INSTALL_PREFIX = $$CHECK_INSTALL_ROOT/usr

# The 'make dist' target
# Creates a tarball
QMAKE_EXTRA_TARGETS += dist
dist.target = dist
dist.commands += git archive HEAD --prefix=$$DIST_NAME/ | bzip2 > $$TARBALL_PATH

# The 'make distcheck' target
# Creates a tarball release, extracts it, builds, runs tests and installs
QMAKE_EXTRA_TARGETS += distcheck
distcheck.target = distcheck
distcheck.depends += dist
distcheck.commands += mkdir -p $$OUT_PWD/distcheck-build;
distcheck.commands += cd $$OUT_PWD/distcheck-build;
distcheck.commands += tar -xf $$TARBALL_PATH;
distcheck.commands += cd $$DIST_NAME;
distcheck.commands += qmake -r \
    GEGLQT_INSTALL_PRIVATE_HEADERS=yes \
    GEGLQT_INSTALL_PREFIX=$$CHECK_INSTALL_PREFIX;
distcheck.commands += make -j4;
distcheck.commands += make check;
distcheck.commands += INSTALL_ROOT=$$CHECK_INSTALL_ROOT make install;

# Transplant the files respecting the install prefix so that
# the files will reside on disk where the install prefix said they would
# (since we used INSTALL_ROOT) they did not do that before
# This is similar to what distribution packaging does, when they transplant
# files from the install root used during build to /
distcheck.commands += rsync -ra --backup \
    $$CHECK_INSTALL_ROOT$$CHECK_INSTALL_ROOT/usr/* $$CHECK_INSTALL_ROOT/usr/;
distcheck.commands += find $$CHECK_INSTALL_ROOT$$CHECK_INSTALL_ROOT/* \
    -depth -exec rmdir \'{}\' \';\' -type d;
distcheck.commands += rmdir -p $$CHECK_INSTALL_ROOT$$CHECK_INSTALL_ROOT || echo ignored ;

# The 'make intcheck' target
# Creates a tarball release, extracts it, builds, runs tests and installs,
# then extracts the tarball release again, and builds and runs test against the installed version
QMAKE_EXTRA_TARGETS += intcheck
intcheck.target = intcheck
intcheck.depends += distcheck;
intcheck.commands += mkdir -p $$OUT_PWD/intcheck-build;
intcheck.commands += cd $$OUT_PWD/intcheck-build;
intcheck.commands += tar -xf $$TARBALL_PATH;
intcheck.commands += cd $$DIST_NAME;
PKG_CONFIG_PATH = $$system(echo $PKG_CONFIG_PATH)
intcheck.commands += \
    PKG_CONFIG_PATH=$$CHECK_INSTALL_PREFIX/lib/pkgconfig:$$PKG_CONFIG_PATH \
    qmake -r GEGLQT_INTEGRATION_CHECK=yes;
intcheck.commands += make -j4;
intcheck.commands += make check;

OTHER_FILES = \
    README.txt gegl-qt.doap \
