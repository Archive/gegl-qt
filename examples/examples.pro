include(../config.pri)
# TODO: install examples

TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS += \
    common \

# Examples that depend on Qt Widgets (optional)
contains(HAVE_QT_WIDGETS, yes) {
    SUBDIRS += \
        qwidget-basic \
        qgv-basic \
        qwidget-transformations \
        qwidget-autotransform \
        display-operation \

}

# Examples that depend on Qt Quick1(optional)
contains(HAVE_QT_QUICK1, yes) {
    SUBDIRS += \
        qml-basic \
        qml-paint \
        qml-operations \
}

OTHER_FILES += \
    python/basic.py \
    python/nodeviewoptions.py \
