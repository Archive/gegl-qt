/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "../common/paint-engine.h"

#include <gegl-qt.h>
#include <gegl-qt-declarative.h>

#include <QtGui>
#include <QtCore>
#include <QtDeclarative>

using namespace GeglQt;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    gegl_init(&argc, &argv);
    Q_INIT_RESOURCE(qmlpaint);

    // Does all the GEGL graph stuff
    PaintEngine paintEngine;

    QDeclarativeView view;
    // Expose the paint engine to QML, so it can be used there
    view.rootContext()->setContextProperty("paintEngine", &paintEngine);
    view.setSource(QUrl("qrc:/qml-paint.qml"));
    view.show();

    int retCode = a.exec();
    gegl_exit();
    return retCode;
}




