import QtQuick 1.0
import GeglQt4 0.1 as GeglQt

Rectangle {
    width: 512
    height: 512

    GeglQt.NodeView {
        id: view
        anchors.fill: parent
        inputNode: paintEngine.outputNode()
        x: 0
        y: 0
    }

    MouseArea {
        anchors.fill: view
        hoverEnabled: true
        onPositionChanged: {
            paintEngine.positionChanged(mouse.x, mouse.y, (mouse.buttons & Qt.LeftButton))
        }
    }
}
