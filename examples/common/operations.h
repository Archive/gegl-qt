#ifndef OPERATIONS_H
#define OPERATIONS_H

#include <QObject>
#include <QtCore>
#include <glib-object.h>

/* OperationProperty:
 *
 * Essentially a value-type. Only a QObject in order to use properties,
 * to be able to expose such objects to QML. */
class OperationProperty : public QObject
{
    Q_OBJECT
public:
    explicit OperationProperty(QString name, QString description, QVariant defaultValue,
                               QVariant minValue, QVariant maxValue);

    QString name();
    QString shortDescription();

    QVariant minValue();
    QVariant maxValue();
    QVariant defaultValue();

    Q_PROPERTY (QString name READ name CONSTANT)
    Q_PROPERTY (QString shortDescription READ shortDescription CONSTANT)
    Q_PROPERTY (QVariant defaultValue READ defaultValue CONSTANT)
    Q_PROPERTY (QVariant minValue READ minValue CONSTANT)
    Q_PROPERTY (QVariant maxValue READ minValue CONSTANT)

private:
    QString mName;
    QString mShortDescription;
    GType mValueType;
    QVariant mDefaultValue;
    QVariant mMinValue;
    QVariant mMaxValue;
};


/* Operation:
 *
 * Essentially a value-type. Only a QObject in order to use properties,
 * to be able to expose such objects to QML. */
class Operation : public QObject
{
    Q_OBJECT
public:
    explicit Operation(QString name, QString description, QStringList categoryList,
                       QMap<QString, OperationProperty *> properties);

    QString name();
    QObjectList properties();
    QString description();
    QStringList categories();

    Q_PROPERTY (QString name READ name CONSTANT)
    Q_PROPERTY (QObjectList properties READ properties CONSTANT)
    Q_PROPERTY (QString description READ description CONSTANT)
    Q_PROPERTY (QStringList categories READ categories CONSTANT)

private:
    QString mName;
    GType mType;
    QStringList mCategories;
    QString mDescription;
    QMap<QString, OperationProperty *> mProperties;
};


class Operations
{

public:
    /* Return all available operations. */
    static QMap<QString, Operation*> all();
};

class OperationsBrowser : public QObject
{
    Q_OBJECT

public:
    explicit OperationsBrowser(QObject *parent = 0);

    Operation *selectedOperation();
    QStringList availableOperations();

    Q_INVOKABLE void selectOperation(const QString &operationName);
    Q_SIGNAL void selectedOperationChanged();

    Q_PROPERTY (QObject * selectedOperation READ selectedOperation NOTIFY selectedOperationChanged)
    Q_PROPERTY (QStringList availableOperations READ availableOperations CONSTANT)

private:
    QString mSelectedOperation;
    QMap<QString, Operation*> mAllOperations;
};

#endif // OPERATIONS_H
