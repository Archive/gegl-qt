/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "paint-engine.h"

#include <gegl.h>

/* Just holds the state used by PaintEngine. */
struct PaintEnginePrivate
{
    GeglNode *graph;
    GeglNode *outputNode;
    GeglNode *strokeNode;
    GeglPath *strokePath;
    bool inStroke;
};

namespace {
    const gchar * const DefaultColor = "black";
    float DefaultHardness = 0.3;
    float DefaultLineWidth = 20.0;
}

/* PaintEngine
 * Trivial painting engine implemented using Gegl.
 * Is not tied to QML, could be used in any toolkit. */
PaintEngine::PaintEngine(QObject *parent)
    : QObject(parent)
    , priv(new PaintEnginePrivate())
{
    priv->graph = gegl_node_new();
    priv->outputNode = gegl_node_new_child(priv->graph,
                                           "operation", "gegl:nop", NULL);
    priv->inStroke = false;

}

PaintEngine::~PaintEngine()
{
    g_object_unref(priv->graph);
    // Nodes are owned by the graph
    delete priv;
}

QVariant
PaintEngine::outputNode()
{
    return qVariantFromValue(static_cast<void*>(priv->outputNode));
}

void
PaintEngine::positionChanged(double x, double y, bool buttonPressed)
{
    if (buttonPressed && !priv->inStroke) {
        // Start new stroke
        GeglNode *oldStrokeNode = priv->strokeNode;
        priv->inStroke = true;

        priv->strokePath = gegl_path_new();
        gegl_path_append (priv->strokePath, 'M', x, y);

        priv->strokeNode = gegl_node_new_child(priv->graph,
                                               "operation", "gegl:path",
                                               "d", priv->strokePath,
                                               "fill-opacity", 0.0,
                                               "stroke", gegl_color_new (DefaultColor),
                                               "stroke-width", DefaultLineWidth,
                                               "stroke-hardness", DefaultHardness,
                                               NULL);
        if (oldStrokeNode) {
            gegl_node_link_many(oldStrokeNode, priv->strokeNode, NULL);
        }
        gegl_node_link_many(priv->strokeNode, priv->outputNode, NULL);

    } else if (buttonPressed && priv->inStroke) {
        // Add a new point to the stroke
        gegl_path_append(priv->strokePath, 'L', x, y, NULL);

    } else if (!buttonPressed && priv->inStroke) {
        // End stroke
        priv->inStroke = false;
        gegl_path_append (priv->strokePath, 'M', x, y);
        g_object_unref (priv->strokePath);

    } else if (!buttonPressed && !priv->inStroke) {
        // Do nothing
    }

}
