#ifndef QMLPAINTENGINE_H
#define QMLPAINTENGINE_H

#include <QtCore>

class PaintEnginePrivate;

class PaintEngine : public QObject
{
    Q_OBJECT
public:
    explicit PaintEngine(QObject *parent = 0);
    ~PaintEngine();

    Q_INVOKABLE QVariant outputNode();
    Q_INVOKABLE void positionChanged(double x, double y, bool buttonPressed);

private:
    void clear();

private:
    PaintEnginePrivate* priv;
};

#endif // QMLPAINTENGINE_H
