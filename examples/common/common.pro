include(../../config.pri)

QT = core
TEMPLATE = lib
CONFIG += staticlib
TARGET = $$GEGLQT_LIBNAME-examples-common

SOURCES += \
    paint-engine.cpp \
    operations.cpp \

HEADERS += \
    paint-engine.h \
    operations.h \

CONFIG += link_pkgconfig
PKGCONFIG += $$GEGL_PKG
