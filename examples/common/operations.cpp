#include "operations.h"

#include <gegl.h>
#include <gegl-plugin.h> // For GeglOperationClass

enum ValueAttribute {
    ValueMax,
    ValueDefault,
    ValueMin
};

QVariant
getValueAttribute(ValueAttribute valueAttribute, GType valueType, GParamSpec *param_spec)
{
    QVariant minValue;
    QVariant maxValue;
    QVariant defaultValue;

    if (g_type_is_a (valueType, G_TYPE_DOUBLE)) {
        const gdouble def = G_PARAM_SPEC_DOUBLE (param_spec)->default_value;
        const gdouble min = G_PARAM_SPEC_DOUBLE (param_spec)->minimum;
        const gdouble max = G_PARAM_SPEC_DOUBLE (param_spec)->maximum;

        defaultValue = QVariant::fromValue(def);
        minValue = QVariant::fromValue(min);
        maxValue = QVariant::fromValue(max);

    } else if (g_type_is_a (valueType, G_TYPE_INT)) {
        const gint def = G_PARAM_SPEC_INT (param_spec)->default_value;
        const gint min = G_PARAM_SPEC_INT (param_spec)->minimum;
        const gint max = G_PARAM_SPEC_INT (param_spec)->maximum;

        defaultValue = QVariant::fromValue(def);
        minValue = QVariant::fromValue(min);
        maxValue = QVariant::fromValue(max);

    } else if (g_type_is_a (valueType, G_TYPE_FLOAT)) {
          const gfloat def = G_PARAM_SPEC_FLOAT (param_spec)->default_value;
          const gfloat min = G_PARAM_SPEC_FLOAT (param_spec)->minimum;
          const gfloat max = G_PARAM_SPEC_FLOAT (param_spec)->maximum;

          defaultValue = QVariant::fromValue(def);
          minValue = QVariant::fromValue(min);
          maxValue = QVariant::fromValue(max);

    } else if (g_type_is_a (valueType, G_TYPE_BOOLEAN)) {
        const gboolean def = G_PARAM_SPEC_BOOLEAN (param_spec)->default_value;
        defaultValue = QVariant::fromValue(def);

    } else if (g_type_is_a (valueType, G_TYPE_STRING)) {
        const gchar *string = G_PARAM_SPEC_STRING (param_spec)->default_value;
        defaultValue = QVariant::fromValue(QString::fromUtf8(string));

    } else {
        // Unknown type
    }

    switch (valueAttribute) {
    case ValueMax:
        return maxValue;
    case ValueMin:
        return minValue;
    case ValueDefault:
    default:
        return defaultValue;

    }
}

QMap<QString,OperationProperty *>
getPropertyMap(const QString &operationName)
{
    QMap<QString,OperationProperty *> propertyMap;

    GParamSpec **properties;
    guint prop_no;
    guint n_properties;

#if GEGL_MINOR_VERSION >= 2
    properties = gegl_operation_list_properties(operationName.toUtf8(), &n_properties);
#else
    properties = gegl_list_properties(operationName.toUtf8(), &n_properties);
#endif

    for (prop_no = 0; prop_no < n_properties; prop_no++)
    {
        GParamSpec *param_spec = properties[prop_no];

        QString name = QString::fromUtf8(g_param_spec_get_name(param_spec));
        QString shortDescription = QString::fromUtf8(g_param_spec_get_blurb(param_spec));
        GType valueType = G_PARAM_SPEC_VALUE_TYPE(param_spec);
        QVariant minValue = getValueAttribute(ValueMin, valueType, param_spec);
        QVariant maxValue = getValueAttribute(ValueMax, valueType, param_spec);
        QVariant defaultValue = getValueAttribute(ValueDefault, valueType, param_spec);

        OperationProperty *prop = new OperationProperty(name, shortDescription,
                                                        defaultValue, minValue, maxValue);
        propertyMap.insert(name, prop);
    }
    return propertyMap;
}

/* */
QList<GeglOperationClass *> *
getOperationsClasses(QList<GeglOperationClass *> *list, GType type)
{
    GeglOperationClass *klass;
    GType *classes;
    guint  no_children;
    guint  no;

    if (!type)
      return list;

    klass = GEGL_OPERATION_CLASS(g_type_class_ref(type));
    if (klass->name != NULL)
      list->append(klass);

    classes = g_type_children(type, &no_children);

    for (no=0; no < no_children; no++) {
        list = getOperationsClasses(list, classes[no]);
    }
    if (classes) {
        g_free(classes);
    }
    return list;
}


QMap<QString, Operation*>
Operations::all()
{
    QMap<QString, Operation*> operationMap;

    QList<GeglOperationClass *> *operations = new QList<GeglOperationClass *>;
    operations = getOperationsClasses(operations, GEGL_TYPE_OPERATION);

    foreach (GeglOperationClass *op_class, *operations) {
        QString name = QString::fromUtf8(op_class->name);

#if GEGL_MINOR_VERSION >= 2
        QString categories = QString::fromUtf8(gegl_operation_get_key(op_class->name, "categories"));
        QString description = QString::fromUtf8(gegl_operation_get_key(op_class->name, "description"));
#else
        QString categories = QString::fromUtf8(op_class->categories);
        QString description = QString::fromUtf8(op_class->description);
#endif

        QStringList categoryList = categories.split(":");
        Operation *operation = new Operation(name, description, categoryList, getPropertyMap(name));
        operationMap.insert(name, operation);
    }

    return operationMap;
}


/* OperationProperty */
OperationProperty::OperationProperty(QString name, QString description, QVariant defaultValue,
                                     QVariant minValue, QVariant maxValue)
    : QObject()
    , mName(name)
    , mShortDescription(description)
    , mDefaultValue(defaultValue)
    , mMinValue(minValue)
    , mMaxValue(maxValue)
{
}

QString OperationProperty::name()
{
    return mName;
}

QString OperationProperty::shortDescription()
{
    return mShortDescription;
}

QVariant OperationProperty::minValue()
{
    return mMinValue;
}

QVariant OperationProperty::maxValue()
{
    return mMaxValue;
}

QVariant OperationProperty::defaultValue()
{
    return mDefaultValue;
}


/* Operation */
Operation::Operation(QString name, QString description,
                     QStringList categoryList, QMap<QString, OperationProperty *> properties)
    : QObject()
    , mName(name)
    , mCategories(categoryList)
    , mDescription(description)
    , mProperties(properties)
{
}

QString Operation::name()
{
    return mName;
}

QString Operation::description()
{
    return mDescription;
}

QStringList Operation::categories()
{
    return mCategories;
}

QObjectList Operation::properties()
{
    QObjectList list;

    QStringList propertyNames = mProperties.keys();
    qSort(propertyNames);

    Q_FOREACH(const QString &propertyName, propertyNames) {
        list.append(mProperties.value(propertyName));
    }

    return list;
}

OperationsBrowser::OperationsBrowser(QObject *parent)
    : QObject(parent)
{
    mAllOperations = Operations::all();
}

void OperationsBrowser::selectOperation(const QString &operationName)
{
    if (mSelectedOperation == operationName) {
        return;
    }
    mSelectedOperation = operationName;
    Q_EMIT selectedOperationChanged();
}

Operation *OperationsBrowser::selectedOperation()
{
    return mAllOperations.value(mSelectedOperation);
}

QStringList OperationsBrowser::availableOperations()
{
    return mAllOperations.keys();
}
