#!/usr/bin/env python2

import sys

import gobject
import gi
from gi.repository import Gegl

from PySide.QtCore import *
from PySide.QtGui import *

import pygeglqt4
pygeglqt4.require("0.1")
from geglqt import GeglQt

graph_xml = """
<gegl>
  <gegl:load path='%s'/>
</gegl>"""

if __name__ == '__main__':

    app = QApplication(sys.argv)

    if len(sys.argv) != 2:
        print "usage: %s FILE" % sys.argv[0]
        exit(1)

    file_path = sys.argv[1]

    Gegl.init(0,"")
    graph = Gegl.Node.new_from_xml(graph_xml % file_path, "/")
    node = graph.get_children()[-1]

    view = GeglQt.NodeViewWidget()

    view.options().setTranslationX(10)
    view.options().setTranslationY(10)

    view.options().setAutoScalePolicy(GeglQt.NodeViewOptions.AutoScaleToView)
    view.options().setAutoCenterPolicy(GeglQt.NodeViewOptions.AutoCenterEnabled)

    view.setInputNode(node)
    view.show()

    app.exec_()
    sys.exit()
