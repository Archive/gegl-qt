#!/usr/bin/python
 
import sys

import gi
from gi.repository import Gegl

from PySide.QtCore import *
from PySide.QtGui import *

import pygeglqt4
pygeglqt4.require("0.1")
from geglqt import GeglQt

graph_xml = """
<gegl>
  <gegl:crop width='512' height='512'/>
  <gegl:over >
    <gegl:translate x='30' y='30'/>
    <gegl:dropshadow radius='1.5' x='3' y='3'/>
    <gegl:text size='80' color='white'><params>
      <param name='string'>GEGL QT</param>
      </params>
    </gegl:text>
  </gegl:over>
  <gegl:unsharp-mask std-dev='30'/>
  <gegl:load path='%s'/>
</gegl>"""

if __name__ == '__main__' :
 
    app = QApplication(sys.argv)
 
    #label = QLabel("Hello World")
    #label.show()
 
    if len(sys.argv) != 2:
        print "usage: %s FILE" % sys.argv[0]
        exit(1)

    file_path = sys.argv[1]

    Gegl.init(0,"")
    node = Gegl.Node.new_from_xml(graph_xml % file_path, "/");

    view = GeglQt.NodeViewWidget()
    view.setInputNode(node.get_children()[-1])
    view.show()

    app.exec_()
    sys.exit()
