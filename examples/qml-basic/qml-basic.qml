import QtQuick 1.0
import GeglQt4 0.1 as GeglQt

GeglQt.NodeView {
    inputNode: globalNode

    height: 500
    width: 500

    options: GeglQt.NodeViewOptions {
        translationX: 0
        translationY: 0
        autoScalePolicy: GeglQt.NodeViewOptions.AutoScaleToView
        autoCenterPolicy: GeglQt.NodeViewOptions.AutoCenterEnabled
    }
}





