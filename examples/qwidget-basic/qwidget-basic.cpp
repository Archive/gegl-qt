/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include <gegl-qt.h>

#include <QApplication>
#include <QtCore>

using namespace GeglQt;

int main(int argc, char *argv[])
{
    GeglNode *graph, *node;

    QApplication a(argc, argv);
    gegl_init(&argc, &argv);

    if (argc < 2) {
        QTextStream(stdout) << "Usage: " << argv[0] << " [options] FILE\n";
        exit(1);
    }

    // Build a Gegl graph that loads a file
    graph = gegl_node_new ();
    node = gegl_node_new_child (graph,
      "operation", "gegl:load",
      "path", argv[argc-1], NULL);
    gegl_node_process(node);

    NodeViewWidget w;
    w.setInputNode(node);
    w.show();

    // FIXME: leaks the gegl graph, does not call gegl_exit
    return a.exec();
}
