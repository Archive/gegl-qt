include(../../config.pri)
include(../examples-common.pri)

SOURCES += \
    qwidget-autotransform.cpp \
    autotransform-controls.cpp

HEADERS += \
    autotransform-controls.h
