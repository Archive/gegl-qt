#ifndef AUTOTRANSFORMCONTROLS_H
#define AUTOTRANSFORMCONTROLS_H

#include <QtGui>
#include <gegl-qt/nodeviewoptions.h>
#include <QWidget>

class QCheckBox;

using namespace GeglQt;

class AutoTransformControls : public QWidget
{
    Q_OBJECT
public:
    explicit AutoTransformControls(QWidget *parent = 0);

    void setViewOptionsInstance(NodeViewOptions*);

private Q_SLOTS:
    void autoCenterChanged(bool newValue);
    void autoScaleChanged(bool newValue);

private:
    void buildUi();

private:
    QCheckBox *m_autoCenterCheckBox;
    QCheckBox *m_autoScaleCheckBox;
    NodeViewOptions *mViewOptions;
};




#endif // AUTOTRANSFORMCONTROLS_H
