/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "autotransform-controls.h"

#include <QCheckBox>
#include <QBoxLayout>

AutoTransformControls::AutoTransformControls(QWidget *parent) :
    QWidget(parent)
{
    mViewOptions = 0;
    buildUi();
}

/* */
void
AutoTransformControls::buildUi()
{
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    layout->addStretch(1);

    m_autoCenterCheckBox = new QCheckBox("AutoCenter");
    layout->addWidget(m_autoCenterCheckBox);

    m_autoScaleCheckBox = new QCheckBox("AutoScale");
    layout->addWidget(m_autoScaleCheckBox);

    this->setLayout(layout);
}

void
AutoTransformControls::setViewOptionsInstance(NodeViewOptions *options)
{
    mViewOptions = options;

    connect(m_autoScaleCheckBox, SIGNAL(toggled(bool)),
            this, SLOT(autoScaleChanged(bool)));
    m_autoScaleCheckBox->setChecked(true);

    connect(m_autoCenterCheckBox, SIGNAL(toggled(bool)),
            this, SLOT(autoCenterChanged(bool)));
    m_autoCenterCheckBox->setChecked(true);
}

void
AutoTransformControls::autoCenterChanged(bool newValue)
{
    NodeViewOptions::AutoCenter autoCenter = newValue ?
                NodeViewOptions::AutoCenterEnabled : NodeViewOptions::AutoCenterDisabled;

    mViewOptions->setAutoCenterPolicy(autoCenter);

    if (autoCenter == NodeViewOptions::AutoCenterDisabled) {
        // To get a nice toggling effect, reset translation
        // Otherwise the view would just have stayed where it is when autocentering is disabled.
        mViewOptions->setTranslationX(0.0);
        mViewOptions->setTranslationY(0.0);
    }
}

void
AutoTransformControls::autoScaleChanged(bool newValue)
{
    NodeViewOptions::AutoScale autoScale = newValue ?
                NodeViewOptions::AutoScaleToView : NodeViewOptions::AutoScaleDisabled;
    mViewOptions->setAutoScalePolicy(autoScale);

    if (autoScale == NodeViewOptions::AutoScaleDisabled) {
        // To get a nice toggling effect, reset scale
        // Otherwise the view would just have stayed where it os when autoscale is disabled.
        mViewOptions->setScale(1.0);
    }
}
