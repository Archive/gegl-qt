isEmpty(GEGLQT_CONFIG):error("config.pri not found")
# This file uses variables from config.pri, so users must include config.pri before including this file

QT += core gui
contains(HAVE_QT_DECLARATIVE, yes) {
    QT += $$QT_DECLARATIVE
}
contains(HAVE_QT_WIDGETS, yes) {
    QT += $$QT_WIDGETS
}

CONFIG += qt

OBJECTS_DIR = .obj
MOC_DIR = .moc

isEmpty(USE_EXTERNAL_GEGLQT) {
    INCLUDEPATH += ../../gegl-qt ../.. # ../.. because public includes have gegl-qt/ prefix
    LIBS += -L../../gegl-qt -l$$GEGLQT_LIBNAME
} else {
    CONFIG += link_pkgconfig
    PKGCONFIG += $$GEGLQT_LIBNAME
}

LIBS += ../../examples/common/lib$$GEGLQT_LIBNAME-examples-common.a

CONFIG += link_pkgconfig
PKGCONFIG += $$GEGL_PKG
