include(../../config.pri)
include(../examples-common.pri)

SOURCES += qml-operations.cpp
RESOURCES += qmloperations.qrc
OTHER_FILES += qml-operations.qml \
    OperationView.qml \
    SingleOperationView.qml
