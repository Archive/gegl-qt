import QtQuick 1.0

Rectangle {
    id: singleOperationView

    Rectangle {
        height: 440
        width: 250

        Text {
            height: 40
            text: "Operation: " + browser.selectedOperation.name
            font.pixelSize: 25
        }
        ListView {
            y: 50
            height: 400
            width: parent.width

            model: browser.selectedOperation.properties
            delegate: propertyDelegate

        }
    }

    Component {
        id: propertyDelegate

        Item {
            height: 80

            Column {
                Text { text: modelData.name }
                Text { text: modelData.shortDescription }
                Text { text: modelData.defaultValue }
                Text { text: modelData.minValue }
                Text { text: modelData.maxValue }
            }

        }

    }
}
