import QtQuick 1.0
// import GeglQt 0.1

Rectangle {
    width: 500
    height: 500

    OperationView {
        id: operationsList
        width: 300 // parent.width / 0.6
    }

    SingleOperationView {
        id: singleOperation
        width: 200 // parent.width / 0.4
        anchors.left: operationsList.right
    }

}
