/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 <jononor@gmail.com>
 */

#include <gegl-qt.h>

#include <QtGui>
#include <QtCore>
#include <QtDeclarative>

#include "../common/operations.h"

int main(int argc, char *argv[])
{ 
    QApplication a(argc, argv);
    gegl_init(&argc, &argv);
    Q_INIT_RESOURCE(qmloperations);

    OperationsBrowser browser;

    browser.selectOperation(browser.availableOperations().at(2));

    QDeclarativeView view;

    view.rootContext()->setContextProperty("browser", &browser);

    view.setSource(QUrl("qrc:/qml-operations.qml"));
    view.show();

    int retCode = a.exec();

    // FIXME: free props
    gegl_exit();
    return retCode;
}





