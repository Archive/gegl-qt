import QtQuick 1.0

Rectangle {

    Text {
        height: 40
        text: "Available operations"
        font.pixelSize: 25
    }

    ListView {
        id: operationsView
        y: 50
        height: 200
        width: parent.width
        model: browser.availableOperations
        delegate: operationListDelegate

        //focus: true
    }

    Component {
        id: operationListDelegate

        Rectangle {
            id: operationListDelegateRoot
            width: parent.width
            height: 15

            Text {
                text: modelData
            }

            MouseArea {
                anchors.fill: operationListDelegateRoot
                onClicked: {
                        console.log("clickity")
                        operationsView.currentIndex = index
                        browser.selectOperation(modelData)
                }
            }

        }
    }

}

