/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "transformation-controls.h"

#include <QDoubleSpinBox>
#include <QBoxLayout>

TransformationControls::TransformationControls(QWidget *parent) :
    QWidget(parent)
{
    mViewOptions = 0;
    buildUi();
}

/* */
void
TransformationControls::buildUi()
{
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    layout->addStretch(1);

    // X translation
    m_xSpinBox = new QDoubleSpinBox(this);
    layout->addWidget(m_xSpinBox);
    m_xSpinBox->setMaximum(1000.0);
    m_xSpinBox->setMinimum(-1000.0);
    m_xSpinBox->setPrefix("X: ");
    m_xSpinBox->setSuffix(" px");
    m_xSpinBox->setDecimals(0);
    m_xSpinBox->setSingleStep(20.0);

    // Y translation
    m_ySpinBox = new QDoubleSpinBox(this);
    layout->addWidget(m_ySpinBox);
    m_ySpinBox->setMaximum(1000.0);
    m_ySpinBox->setMinimum(-1000.0);
    m_ySpinBox->setPrefix("Y: ");
    m_ySpinBox->setSuffix(" px");
    m_ySpinBox->setDecimals(0);
    m_ySpinBox->setSingleStep(20.0);

    // Scale
    m_scaleSpinBox = new QDoubleSpinBox(this);
    layout->addWidget(m_scaleSpinBox);
    m_scaleSpinBox->setMaximum(5.0);
    m_scaleSpinBox->setMinimum(0.2);
    m_scaleSpinBox->setPrefix("Scale: ");
    m_scaleSpinBox->setSuffix(" x");
    m_scaleSpinBox->setSingleStep(0.2);

    this->setLayout(layout);
}

void
TransformationControls::setViewOptionsInstance(NodeViewOptions *options)
{
    mViewOptions = options;

    connect(m_xSpinBox, SIGNAL(valueChanged(double)),
            mViewOptions, SLOT(setTranslationX(double)));
    m_xSpinBox->setValue(10.0);

    connect(m_ySpinBox, SIGNAL(valueChanged(double)),
            mViewOptions, SLOT(setTranslationY(double)));
    m_ySpinBox->setValue(10.0);

    connect(m_scaleSpinBox, SIGNAL(valueChanged(double)),
            mViewOptions, SLOT(setScale(double)));
    m_scaleSpinBox->setValue(1.0);
}
