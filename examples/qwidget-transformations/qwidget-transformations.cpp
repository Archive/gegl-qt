/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include <gegl-qt.h>

#include "transformation-controls.h"

#include <QApplication>
#include <QtCore>
#include <QBoxLayout>

int main(int argc, char *argv[])
{
    GeglNode *graph, *node;

    QApplication a(argc, argv);
    gegl_init(&argc, &argv);

    if (argc < 2) {
        QTextStream(stdout) << "Usage: " << argv[0] << " [options] FILE\n";
        exit(1);
    }

    // Build a Gegl graph that loads a file
    graph = gegl_node_new ();
    node = gegl_node_new_child (graph,
      "operation", "gegl:load",
      "path", argv[argc-1], NULL);
    gegl_node_process(node);

    QWidget toplevelWidget;
    QBoxLayout *layout = new QBoxLayout(QBoxLayout::TopToBottom);
    toplevelWidget.setLayout(layout);

    NodeViewWidget *view = new NodeViewWidget();
    view->setInputNode(node);
    layout->addWidget(view, 1);

    TransformationControls *controls = new TransformationControls();
    NodeViewOptions *options = view->options();
    controls->setViewOptionsInstance(options);
    layout->addWidget(controls);

    toplevelWidget.resize(600, 600);
    toplevelWidget.show();

    int retval = a.exec();
    g_object_unref(graph);
    gegl_exit();
    return retval;
}
