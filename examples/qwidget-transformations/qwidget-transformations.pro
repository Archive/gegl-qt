include(../../config.pri)
include(../examples-common.pri)

SOURCES += \
    qwidget-transformations.cpp \
    transformation-controls.cpp

HEADERS += \
    transformation-controls.h
