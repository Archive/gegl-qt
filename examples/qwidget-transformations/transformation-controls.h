#ifndef TRANSFORMATIONCONTROLS_H
#define TRANSFORMATIONCONTROLS_H

#include <gegl-qt/nodeviewoptions.h>

#include <QtGui>
#include <QWidget>

class QDoubleSpinBox;

using namespace GeglQt;

class TransformationControls : public QWidget
{
    Q_OBJECT
public:
    explicit TransformationControls(QWidget *parent = 0);

    void setViewOptionsInstance(NodeViewOptions *options);

private:
    void buildUi();
private:
    NodeViewOptions *mViewOptions;
    QDoubleSpinBox *m_xSpinBox;
    QDoubleSpinBox *m_ySpinBox;
    QDoubleSpinBox *m_scaleSpinBox;
};

#endif // TRANSFORMATIONCONTROLS_H
