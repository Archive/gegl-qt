/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include <gegl.h>

#include <QApplication>
#include <QtCore>

int main(int argc, char *argv[])
{
    GeglNode *graph, *load, *display;

    QApplication a(argc, argv);
    gegl_init(&argc, &argv);

    if (argc < 2) {
        QTextStream(stdout) << "Usage: " << argv[0] << " [options] FILE\n";
        exit(1);
    }

    // Build a Gegl graph that loads a file
    graph = gegl_node_new ();
    load = gegl_node_new_child (graph,
      "operation", "gegl:load",
      "path", argv[argc-1], NULL);
    display = gegl_node_new_child (graph,
      "operation", "gegl-qt4:display", NULL);
    gegl_node_link_many (load, display, NULL);

    gegl_node_process(display);

    const int retval = a.exec();

    g_object_unref(graph);
    gegl_exit();
    return retval;
}
