include(../../config.pri)

QT += core gui
contains(HAVE_QT_WIDGETS, yes) {
    QT += $$QT_WIDGETS
}

SOURCES += display-operation.cpp

# Does not link against gegl-qt as it is not used directly
CONFIG += link_pkgconfig
PKGCONFIG += $$GEGL_PKG
