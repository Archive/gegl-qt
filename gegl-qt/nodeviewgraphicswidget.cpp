/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "nodeviewgraphicswidget.h"
#include "internal/nodeviewimplementation.h"

using namespace GeglQt;

NodeViewGraphicsWidget::NodeViewGraphicsWidget(QGraphicsItem * parent)
    : QGraphicsWidget(parent)
    , priv(new NodeViewImplementation(NodeViewImplementation::DrawModeChildGraphicsItem))
{
    priv->childItem()->setParentItem(static_cast<QGraphicsItem *>(this));

    connect(priv, SIGNAL(viewportSizeRequest(QSizeF)),
            this, SLOT(viewportSizeChangeRequested(QSizeF)));

    connect(this, SIGNAL(geometryChanged()),
            this, SLOT(handleGeometryChange()));
}


NodeViewGraphicsWidget::~NodeViewGraphicsWidget()
{
    delete priv;
}

void
NodeViewGraphicsWidget::setInputNode(GeglNode *node)
{
    priv->setInputNode(node);
}

GeglNode *
NodeViewGraphicsWidget::inputNode()
{
    return priv->inputNode();
}

NodeViewOptions *
NodeViewGraphicsWidget::options() const
{
    return priv->options();
}

void
NodeViewGraphicsWidget::invalidate(QRectF rect)
{
    // Handled by implementation
    Q_UNUSED(rect);
}

void
NodeViewGraphicsWidget::viewportSizeChangeRequested(QSizeF requestedSize)
{
    resize(requestedSize);
}

void
NodeViewGraphicsWidget::paint(QPainter *painter,
                                const QStyleOptionGraphicsItem *option,
                                QWidget *widget)
{
    // Handled by implementation
    Q_UNUSED(widget);
    Q_UNUSED(option);
    Q_UNUSED(painter);
}

void
NodeViewGraphicsWidget::handleGeometryChange()
{
    priv->viewportSizeChanged(geometry().size());
}
