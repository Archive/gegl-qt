/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "nodeviewoptions.h"
#include "internal/nodeviewimplementation.h"

#include <QtGui>

using namespace GeglQt;

struct GeglQtViewOptionsPrivate {
    double translationX;
    double translationY;
    double scale;
    NodeViewOptions::AutoCenter autoCenter;
    NodeViewOptions::AutoScale autoScale;
};

/* TODO: support rotation */
NodeViewOptions::NodeViewOptions(QObject *parent)
    : QObject(parent)
    , priv(new GeglQtViewOptionsPrivate())
{
    // Defaults
    setScale(1.0);
    setTranslationX(0.0);
    setTranslationY(0.0);
    setAutoCenterPolicy(AutoCenterDisabled);
    setAutoScalePolicy(AutoScaleDisabled);
}

void
NodeViewOptions::setScale(double newScale)
{
    if (priv->scale == newScale) {
        return;
    }
    priv->scale = newScale;
    Q_EMIT scaleChanged();
    Q_EMIT transformationChanged();
}

double
NodeViewOptions::scale()
{
    return priv->scale;
}

void
NodeViewOptions::setTranslationX(double newTranslationX)
{
    if (priv->translationX == newTranslationX) {
        return;
    }
    priv->translationX = newTranslationX;
    Q_EMIT translationChanged();
    Q_EMIT transformationChanged();
}

double
NodeViewOptions::translationX()
{
    return priv->translationX;
}

void
NodeViewOptions::setTranslationY(double newTranslationY)
{
    if (priv->translationY == newTranslationY) {
        return;
    }
    priv->translationY = newTranslationY;
    Q_EMIT translationChanged();
    Q_EMIT transformationChanged();
}

double
NodeViewOptions::translationY()
{
    return priv->translationY;
}

QTransform
NodeViewOptions::transformation()
{
    QTransform transform;
    transform.translate(translationX(), translationY());
    transform.scale(scale(), scale());
    return transform;
}


void
NodeViewOptions::setAutoCenterPolicy(AutoCenter newAutoCenter)
{
    if (priv->autoCenter == newAutoCenter) {
        return;
    }
    priv->autoCenter = newAutoCenter;
    Q_EMIT autoCenterPolicyChanged();
}

void
NodeViewOptions::setAutoScalePolicy(AutoScale newAutoScale)
{
    if (priv->autoScale == newAutoScale) {
        return;
    }
    priv->autoScale = newAutoScale;
    Q_EMIT autoScalePolicyChanged();
}

NodeViewOptions::AutoCenter
NodeViewOptions::autoCenterPolicy()
{
    return priv->autoCenter;
}

NodeViewOptions::AutoScale
NodeViewOptions::autoScalePolicy()
{
    return priv->autoScale;
}
