/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "nodeviewwidget.h"
#include "internal/nodeviewimplementation.h"

#include <QtGui>
#include <QtDebug>

using namespace GeglQt;

/* GeglQtView is a QWidget that displays the output of a single GeglNode. */

/* Responsible for drawing the contents of the GeglNode, and
 * providing the available public API to consumers.
 *
 * Basic operation:
 * - Update the widget on redraws signaled by the implementation
 * - Draw the node to the widgets in paintEvent
 *
 * Can be tested by
 * Note that this would also test the implementation class, which is not ideal. */
NodeViewWidget::NodeViewWidget(QWidget *parent)
    : QWidget(parent),
      priv(new NodeViewImplementation())
{
    connect(priv, SIGNAL(viewAreaChanged(QRectF)),
            this, SLOT(invalidate(QRectF)));
    connect(priv, SIGNAL(viewportSizeRequest(QSizeF)),
            this, SLOT(viewportSizeChangeRequested(QSizeF)));
}

NodeViewWidget::~NodeViewWidget()
{
    delete priv;
}

GeglNode *
NodeViewWidget::inputNode() const
{
    return priv->inputNode();
}

void
NodeViewWidget::setInputNode(GeglNode *newNode)
{
    priv->setInputNode(newNode);
}

NodeViewOptions *
NodeViewWidget::options() const
{
    return priv->options();
}

void
NodeViewWidget::invalidate(QRectF rect)
{
    if (rect.isValid()) {
        update(rect.toRect());
    } else {
        update();
    }
}

void
NodeViewWidget::viewportSizeChangeRequested(QSizeF requestedSize)
{
    resize(requestedSize.toSize());
}

// Draw the view of GeglNode onto the widget
void
NodeViewWidget::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    priv->paint(&painter, QRectF(event->rect()));
}

void NodeViewWidget::resizeEvent(QResizeEvent *event)
{
    priv->viewportSizeChanged(event->size());
}
