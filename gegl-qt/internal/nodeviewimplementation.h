#ifndef GEGLQT_NODEVIEWIMPLEMENTATION_H
#define GEGLQT_NODEVIEWIMPLEMENTATION_H

/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include <QtGui>
#include <gegl.h>

#include <gegl-qt/nodeviewoptions.h>

class QGraphicsItem;

namespace GeglQt {

class NodeViewChildItem;

class NodeViewImplementation : public QObject
{
    Q_OBJECT

public:
    enum DrawMode {
        DrawModeExplicitPaint,
        DrawModeChildGraphicsItem
    };

public:
    explicit NodeViewImplementation(DrawMode drawTransform = DrawModeExplicitPaint, QObject *parent = 0);
    ~NodeViewImplementation();

    void setInputNode(GeglNode *node);
    GeglNode *inputNode() const;

    GeglQt::NodeViewOptions *options() const;
    void setOptions(GeglQt::NodeViewOptions *newOptions);

    void paint(QPainter *painter, const QRectF & viewRect);
    QGraphicsItem *childItem();

public Q_SLOTS:
    void viewportSizeChanged(QSizeF newSize);

public: // Only public because invalidate_event and computed_event needs them.
    void nodeInvalidated(GeglRectangle *rect);
    void nodeComputed(GeglRectangle *rect);

Q_SIGNALS:
    void viewAreaChanged(QRectF area);
    void viewportSizeRequest(QSizeF requestedSize);

private Q_SLOTS:
    void processNode();
    void transformationChanged();
    void updateAutoCenterScale();
    void handleScaleChanged();
    void handleTranslationChanged();
    void handleViewAreaChanged(QRectF rect);

private:
    void modelAreaChanged();
    QTransform drawTransformation();
    float drawScale();

private:
    GeglNode *mInputNode; // The node the widget displays.
    GeglProcessor *processor; // Used to process the GeglNode when invalidated.
    QTimer *timer; // Used to defer operations to the mainloop.
    GeglQt::NodeViewOptions *mOptions;
    QSizeF mViewportSize;
    DrawMode mDrawMode;
    NodeViewChildItem *mChildItem;
};

}

#endif // GEGLQT_NODEVIEWIMPLEMENTATION_H
