/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "nodeviewchilditem.h"
#include "internal/nodeviewimplementation.h"

#include <QtDebug> // TEMP
#include <QStyleOptionGraphicsItem>

using namespace GeglQt;

NodeViewChildItem::NodeViewChildItem(NodeViewImplementation *helper, QGraphicsItem *parent=0)
    : QGraphicsObject()
    , mHelper(helper)
    , mItemSize(0.0, 0.0)
{
    Q_UNUSED(parent);
}

NodeViewChildItem::~NodeViewChildItem()
{
}

void
NodeViewChildItem::updateSize(QSizeF newSize)
{
    if (!newSize.isValid()) {
        return;
    }

    // Must be called before changing the return value of boundingRect()
    prepareGeometryChange();
    mItemSize = newSize;
}

QRectF
NodeViewChildItem::boundingRect() const
{
    return QRectF(0.0, 0.0, mItemSize.width(), mItemSize.height());
}

void
NodeViewChildItem::paint(QPainter *painter,
                             const QStyleOptionGraphicsItem *option,
                             QWidget *widget)
{
    Q_UNUSED(widget);

    mHelper->paint(painter, option->exposedRect);
}

