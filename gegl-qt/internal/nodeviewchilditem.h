#ifndef NODEVIEWCHILDITEM_H
#define NODEVIEWCHILDITEM_H

/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include <QGraphicsItem>

namespace GeglQt {

class NodeViewImplementation;

class NodeViewChildItem : public QGraphicsObject
{
public:
    NodeViewChildItem(NodeViewImplementation *helper, QGraphicsItem *parent);
    ~NodeViewChildItem();

    //! reimpl
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    QRectF boundingRect() const;
    //! reimpl end

    void updateSize(QSizeF size);

private:
    NodeViewImplementation *mHelper;
    QSizeF mItemSize;
};

}

#endif // NODEVIEWCHILDITEM_H
