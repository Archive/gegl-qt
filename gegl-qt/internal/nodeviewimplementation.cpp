/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "nodeviewimplementation.h"
#include "internal/nodeviewchilditem.h"

#include <QtCore>
#include <QTransform>

using namespace GeglQt;

/* Utility functions */
namespace {
    void gegl_rectangle_set_from_qrect(GeglRectangle *rect, const QRect &qrect)
    {
        rect->x = qrect.x();
        rect->y = qrect.y();
        rect->width = qrect.width();
        rect->height = qrect.height();
    }

    void qrect_set_from_gegl_rectangle(QRect &qrect, const GeglRectangle *rect)
    {
        qrect.setX(rect->x);
        qrect.setY(rect->y);
        qrect.setWidth(rect->width);
        qrect.setHeight(rect->height);
    }

    /* TODO: handle the clipping caused by the viewport size */
    QRectF viewAreaFromModelArea(const QRectF & modelArea, QTransform transform)
    {
        return transform.mapRect(modelArea);
    }

    QRectF modelAreaFromViewArea(const QRectF & viewArea, QTransform transform)
    {
        Q_ASSERT(transform.isInvertible());
        transform = transform.inverted();
        return transform.mapRect(viewArea);
    }
}

/* Just forward to the implementation class. */
static void
invalidated_event(GeglNode *node, GeglRectangle *rect, NodeViewImplementation *impl)
{
    Q_UNUSED(node);
    impl->nodeInvalidated(rect);
}

/* Just forward to the implementation class. */
static void
computed_event(GeglNode *node, GeglRectangle *rect, NodeViewImplementation *impl)
{
    Q_UNUSED(node);
    impl->nodeComputed(rect);
}

/*
* Reponsible for responding to changes in the GeglNode
* Basic operation:
* - Connects to the 'invalidated' and 'computed' signals on the GeglNode
* - On 'invalidated', processes the GeglNode using a GeglProcessor
* - This results in 'computed' events on the node
* - For each 'computed' events, a 'viewAreaChanged' signal is emitted
*
* Can be tested by:
* - emitting 'invalidated' on the input node, observing that the 'computed' signal is emitted correctly
* - emitting 'computed' on the input node, observing that the 'viewAreaChanged' signal is emitted correctly
*/
NodeViewImplementation::NodeViewImplementation(DrawMode drawTransform, QObject *parent)
    : QObject(parent)
    , mInputNode(0)
    , processor(0)
    , timer(new QTimer())
    , mOptions(0)
    , mViewportSize(-1.0, -1.0)
    , mDrawMode(drawTransform)
    , mChildItem(mDrawMode == DrawModeChildGraphicsItem ? new NodeViewChildItem(this, 0) : 0)
{
    setOptions(0); // default

    connect(timer, SIGNAL(timeout()), this, SLOT(processNode()));

    if (mDrawMode == DrawModeChildGraphicsItem) {
        connect(this, SIGNAL(viewAreaChanged(QRectF)),
                this, SLOT(handleViewAreaChanged(QRectF)));
    }
}

NodeViewImplementation::~NodeViewImplementation()
{
    delete timer;
    if (processor) {
        g_object_unref(processor);
    }
    delete mOptions;
    delete mChildItem;
}

QTransform
NodeViewImplementation::drawTransformation()
{
    return mDrawMode == DrawModeExplicitPaint ? options()->transformation() : QTransform();
}

float
NodeViewImplementation::drawScale()
{
    return mDrawMode == DrawModeExplicitPaint ? options()->scale() : 1.0;
}

void
NodeViewImplementation::setInputNode(GeglNode *node)
{
    if (mInputNode) {
        g_object_unref(mInputNode);
    }
    if (processor) {
        g_object_unref(processor);
        processor = 0;
    }

    mInputNode = node;
    g_object_ref(mInputNode);

    g_signal_connect(mInputNode, "computed",
                     G_CALLBACK (computed_event), this);
    g_signal_connect(mInputNode, "invalidated",
                     G_CALLBACK (invalidated_event), this);

    Q_EMIT viewAreaChanged(QRectF(0.0, 0.0, -1.0, -1.0)); // Redraws everything
}

GeglNode *
NodeViewImplementation::inputNode() const
{
    return mInputNode;
}

NodeViewOptions *
NodeViewImplementation::options() const
{
    return mOptions;
}

void
NodeViewImplementation::setOptions(NodeViewOptions *newOptions)
{
    if (mOptions && mOptions == newOptions) {
        return;
    }

    /* XXX: Don't take complete ownership of the options instance, use QSharedPointer instead? */
    delete mOptions;
    mOptions = newOptions ? newOptions : new NodeViewOptions();

    connect(options(), SIGNAL(transformationChanged()),
            this, SLOT(transformationChanged()));

    connect(options(), SIGNAL(autoCenterPolicyChanged()),
            this, SLOT(updateAutoCenterScale()));
    connect(options(), SIGNAL(autoScalePolicyChanged()),
            this, SLOT(updateAutoCenterScale()));

    if (mDrawMode == DrawModeChildGraphicsItem) {

        connect(options(), SIGNAL(scaleChanged()),
                this, SLOT(handleScaleChanged()));
        connect(options(), SIGNAL(translationChanged()),
                this, SLOT(handleTranslationChanged()));
    }


    Q_EMIT viewAreaChanged(QRectF(0.0, 0.0, -1.0, -1.0)); // Redraws everything
}

/*
 * Depends on three things
 * - Model bounding box
 * - Viewport bounding box
 * - Autoscale and Autocenter options
 */
void
NodeViewImplementation::updateAutoCenterScale()
{
    if (!mViewportSize.isValid() || !inputNode()) {
        return;
    }

    if (options()->autoScalePolicy() == NodeViewOptions::AutoScaleToView) {
        // Set scale such that the full model fits inside the viewport
        GeglRectangle bbox = gegl_node_get_bounding_box(inputNode());
        QRectF modelArea(bbox.x, bbox.y, bbox.width, bbox.height);

        QSizeF view(options()->transformation().mapRect(modelArea).size());
        QSizeF viewport(mViewportSize);

        float widthRatio = view.width() / viewport.width();
        float heightRatio = view.height() / viewport.height();
        float maxRatio = widthRatio >= heightRatio ? widthRatio : heightRatio;

        options()->setScale(options()->scale()*(1.0/maxRatio));

    } else if (options()->autoScalePolicy() == NodeViewOptions::AutoScaleViewport) {
        // Reset transformation, and request that the widget changes its viewport size to fit the model
        options()->setScale(1.0);
        options()->setTranslationX(0.0);
        options()->setTranslationY(0.0);

        GeglRectangle bbox = gegl_node_get_bounding_box(inputNode());
        Q_EMIT viewportSizeRequest(QSizeF(bbox.width, bbox.height));
    }

    if (options()->autoCenterPolicy() == NodeViewOptions::AutoCenterEnabled
        && options()->autoScalePolicy() != NodeViewOptions::AutoScaleViewport) {
        // Set translation such that the center of the model is center of the viewport
        GeglRectangle bbox = gegl_node_get_bounding_box(inputNode());
        QPointF modelCenter(bbox.width/2.0, bbox.height/2.0);

        QPointF viewCenter(options()->transformation().map(modelCenter));
        QPointF viewportCenter(mViewportSize.width()/2.0, mViewportSize.height()/2.0);

        options()->setTranslationX(options()->translationX() + viewportCenter.x() - viewCenter.x());
        options()->setTranslationY(options()->translationY() + viewportCenter.y() - viewCenter.y());
    }
}

/* The model->view transformation changed
 * we must recalculate the viewport */
void
NodeViewImplementation::transformationChanged()
{
    /* OPTIMIZE: On simple transformation changes like
     * translation, only a portion of the viewport actually needs to be redrawn. */

    QRectF invalidRect(0.0, 0.0, -1.0, -1.0);
    Q_ASSERT(!invalidRect.isValid());
    Q_EMIT viewAreaChanged(invalidRect); // Redraws everything
}

/* */
void
NodeViewImplementation::viewportSizeChanged(QSizeF newSize)
{
    mViewportSize = newSize;

    if (mChildItem) {
        mChildItem->updateSize(newSize);
    }

    updateAutoCenterScale();
}

/* An area in the GeglNode has been invalidated,
 * process it to compute the contents. */
void
NodeViewImplementation::nodeInvalidated(GeglRectangle *rect)
{
    if (!mInputNode) {
        return;
    }

    if (processor) {
        gegl_processor_set_rectangle(processor, rect);
    } else {
        processor = gegl_node_new_processor(mInputNode, rect);
    }

    /* The actual processing is deferred to the mainloop. */
    timer->start();
}

void
NodeViewImplementation::processNode()
{
    if (!processor) {
        return;
    }

    if (!gegl_processor_work(processor, NULL)) {
        // All work is done
        timer->stop();
    }
}

void
NodeViewImplementation::nodeComputed(GeglRectangle *rect)
{
    updateAutoCenterScale(); // Notify of potential changes in the GeglNode bounding box

    QRect modelRect;
    qrect_set_from_gegl_rectangle(modelRect, rect);
    QRectF viewArea = viewAreaFromModelArea(QRectF(modelRect), drawTransformation());
    Q_EMIT viewAreaChanged(viewArea);
}


void
NodeViewImplementation::paint(QPainter *painter, const QRectF & viewRect)
{
    guchar *buffer = NULL;
    GeglRectangle roi;

    const Babl *format = babl_format("B'aG'aR'aA u8");

    if (!format) {
        qCritical() << "Unknown Babl format";
        return;
    }

    QRectF modelRect = modelAreaFromViewArea(viewRect, drawTransformation());
    gegl_rectangle_set_from_qrect(&roi, modelRect.toRect());

    if (!inputNode() || roi.width <= 0 || roi.height <= 0) {
        return;
    }

    buffer = (guchar *)g_malloc ((roi.width) * (roi.height) * 4);

    gegl_node_blit (inputNode(), 1.0, &roi,
                    format, (gpointer)buffer,
                    GEGL_AUTO_ROWSTRIDE, GEGL_BLIT_CACHE);

    QImage image(buffer, roi.width, roi.height, roi.width*4,
                 QImage::QImage::Format_ARGB32_Premultiplied);

    painter->scale(drawScale(), drawScale());
    painter->drawImage(QPointF(), image);

    g_free(buffer);
}


void
NodeViewImplementation::handleScaleChanged()
{
    float newScale = options()->scale();

    if (!mChildItem || mChildItem->scale() == newScale) {
        return;
    }

    mChildItem->setScale(newScale);
}

void
NodeViewImplementation::handleTranslationChanged()
{
    float newX = options()->translationX();
    float newY = options()->translationY();

    if (!mChildItem || mChildItem->x() == newX || mChildItem->y() == newY) {
        return;
    }

    mChildItem->setPos(newX, newY);
}

QGraphicsItem *
NodeViewImplementation::childItem()
{
    return mChildItem;
}

void
NodeViewImplementation::handleViewAreaChanged(QRectF rect)
{
    if (!mChildItem) {
        return;
    }

    if (rect.isValid()) {
        mChildItem->update(rect);
    } else {
        mChildItem->update(mChildItem->boundingRect());
    }
}
