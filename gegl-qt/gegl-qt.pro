include(../config.pri)

TARGET = $$GEGLQT_LIBNAME
target.path = $$GEGLQT_INSTALL_LIBS
TEMPLATE = lib
CONFIG += qt no_keywords

VERSION = $$GEGLQT_VERSION

QT += core gui
contains(HAVE_QT_DECLARATIVE, yes) {
    QT += $$QT_DECLARATIVE
}
contains(HAVE_QT_WIDGETS, yes) {
    QT += $$QT_WIDGETS
}

CONFIG += link_pkgconfig
PKGCONFIG += $$GEGL_PKG

OBJECTS_DIR = .obj
MOC_DIR = .moc

PUBLIC_SOURCES += \
    nodeviewoptions.cpp \

PUBLIC_HEADERS += \
    nodeviewoptions.h \

TOP_HEADERS += gegl-qt.h GeglQt

#PRIVATE_HEADERS += \

#PRIVATE_SOURCES += \

contains(HAVE_QT_WIDGETS, yes) {
    PUBLIC_SOURCES += \
        nodeviewwidget.cpp \
        nodeviewgraphicswidget.cpp \

    PUBLIC_HEADERS += \
        nodeviewwidget.h \
        nodeviewgraphicswidget.h \

    # FIXME: remove QtWidgets dependency from NodeViewImplementation
    PRIVATE_HEADERS += \
        internal/nodeviewimplementation.h \
        internal/nodeviewchilditem.h \

    PRIVATE_SOURCES += \
        internal/nodeviewimplementation.cpp \
        internal/nodeviewchilditem.cpp \

    TOP_HEADERS += gegl-qt-widgets.h GeglQtWidgets
}

# Code that depends on Qt Declarative (optional)
contains(HAVE_QT_DECLARATIVE, yes) {

    contains(QT_MAJOR_VERSION, 5) {
        #PUBLIC_HEADERS +=
        #PUBLIC_SOURCES +=
    }

    contains(HAVE_QT_QUICK1, yes) {
        PUBLIC_HEADERS += nodeviewdeclarativeitem.h
        PUBLIC_SOURCES += nodeviewdeclarativeitem.cpp
    }

    TOP_HEADERS += gegl-qt-declarative.h GeglQtDeclarative
}

SOURCES += $$PUBLIC_SOURCES $$PRIVATE_SOURCES

HEADERS += $$PUBLIC_HEADERS $$PRIVATE_HEADERS

INCLUDEPATH += .. # Public includes have gegl-qt/ prefix

headers.path = $$GEGLQT_INSTALL_HEADERS/$$GEGLQT_LIBNAME/$$GEGLQT_PROJECTNAME
headers.files = $$PUBLIC_HEADERS

private_headers.files = $$PRIVATE_HEADERS
private_headers.path = $$GEGLQT_INSTALL_HEADERS/$$GEGLQT_LIBNAME/$$GEGLQT_PROJECTNAME/internal

top_headers.path = $$GEGLQT_INSTALL_HEADERS/$$GEGLQT_LIBNAME
top_headers.files = $$TOP_HEADERS

outputFiles(gegl-qt$$QT_MAJOR_VERSION-$${GEGLQT_API_VERSION}.pc)

pkgconfig.path = $$GEGLQT_INSTALL_LIBS/pkgconfig
pkgconfig.files = gegl-qt$$QT_MAJOR_VERSION-$${GEGLQT_API_VERSION}.pc

INSTALLS += target headers top_headers pkgconfig
!isEmpty(GEGLQT_INSTALL_PRIVATE_HEADERS) {
    INSTALLS += private_headers
}

OTHER_FILES += \
    gegl-qt$$QT_MAJOR_VERSION-$${GEGLQT_API_VERSION}.pc.in \

