#ifndef GEGLQT_NODEVIEWGRAPHICSWIDGET_H
#define GEGLQT_NODEVIEWGRAPHICSWIDGET_H

/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include <QGraphicsWidget>
#include <gegl.h>

#include <gegl-qt/nodeviewoptions.h>

typedef GeglNode * GeglNodePtr;

namespace GeglQt {

class NodeViewImplementation;

/*!
 * QGraphicsWidget showing the output of a GeglNode
 */

class NodeViewGraphicsWidget : public QGraphicsWidget
{
    Q_OBJECT
public:
    explicit NodeViewGraphicsWidget(QGraphicsItem *parent = 0);
    ~NodeViewGraphicsWidget();

    /*! The node this widget displays.
     * \return The GeglNode
     */
    GeglNodePtr inputNode();

    /*! Set the node to display
     * \param node The GeglNode to display, or 0 to unset
     *
     * Will reference the GeglNode.
     */
    void setInputNode(GeglNodePtr node);

    /*! The options used on this widget. */
    GeglQt::NodeViewOptions *options() const;

public:
    //! \internal
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private Q_SLOTS:
    void invalidate(QRectF rect);
    void viewportSizeChangeRequested(QSizeF);

    void handleGeometryChange();

private:
    Q_DISABLE_COPY(NodeViewGraphicsWidget)
    NodeViewImplementation *priv;
};

}
#endif // GEGLQT_NODEVIEWGRAPHICSWIDGET_H
