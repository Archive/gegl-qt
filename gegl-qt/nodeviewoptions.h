#ifndef GEGLQT_NODEVIEWOPTIONS_H
#define GEGLQT_NODEVIEWOPTIONS_H

/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include <QtCore>

class GeglQtViewOptionsPrivate;

namespace GeglQt
{

/*!
 * Options available for NodeView widgets
 *
 * Note: when enabling autoCenter/autoScaling the values for translation/scale
 * will be automatically managed. Values set manually will be overwritten.
 *
 * \sa NodeViewWidget, NodeViewGraphicsWidget, NodeViewDeclarativeItem
 */

class NodeViewOptions : public QObject
{
    Q_OBJECT
    Q_ENUMS(AutoCenter)
    Q_ENUMS(AutoScale)

public:
    explicit NodeViewOptions(QObject *parent = 0);

public:
    enum AutoCenter {
      AutoCenterDisabled,
      AutoCenterEnabled
    };

    enum AutoScale {
      AutoScaleDisabled,
      AutoScaleToView,
      AutoScaleViewport
    };

public Q_SLOTS:
    void setScale(double newScale);
    void setTranslationX(double newTranslationX);
    void setTranslationY(double newTranslationY);

    void setAutoCenterPolicy(AutoCenter newAutoCenter);
    void setAutoScalePolicy(AutoScale newAutoScale);

public:
    double scale();
    double translationX();
    double translationY();
    QTransform transformation();

    AutoCenter autoCenterPolicy();
    AutoScale autoScalePolicy();

public:
    Q_PROPERTY(double scale
               READ scale WRITE setScale NOTIFY scaleChanged)
    Q_PROPERTY(double translationX
               READ translationX WRITE setTranslationX NOTIFY translationChanged)
    Q_PROPERTY(double translationY
               READ translationY WRITE setTranslationY NOTIFY translationChanged)

    Q_PROPERTY(AutoCenter autoCenterPolicy
               READ autoCenterPolicy WRITE setAutoCenterPolicy NOTIFY autoCenterPolicyChanged)
    Q_PROPERTY(AutoScale autoScalePolicy
               READ autoScalePolicy WRITE setAutoScalePolicy NOTIFY autoScalePolicyChanged)

Q_SIGNALS:
    void translationChanged();
    void scaleChanged();
    void transformationChanged();

    void autoCenterPolicyChanged();
    void autoScalePolicyChanged();

private:
    Q_DISABLE_COPY(NodeViewOptions)
    GeglQtViewOptionsPrivate *priv;
};

}

Q_DECLARE_METATYPE(GeglQt::NodeViewOptions::AutoCenter)
Q_DECLARE_METATYPE(GeglQt::NodeViewOptions::AutoScale)

#endif // GEGLQT_NODEVIEWOPTIONS_H
