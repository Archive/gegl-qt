#ifndef GEGLQT_NODEVIEWDECLARATIVEITEM_H
#define GEGLQT_NODEVIEWDECLARATIVEITEM_H

/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include <QtDeclarative>
#include <QDeclarativeItem>
#include <QtCore>
#include <gegl.h>

#include <gegl-qt/nodeviewoptions.h>

typedef GeglNode * GeglNodePtr;

namespace GeglQt {

class NodeViewImplementation;

/*!
 * QDeclarativeItem showing the output of a GeglNode
 *
 * Typically used from QML, but can also be instantiated by C++.
 */

class NodeViewDeclarativeItem : public QDeclarativeItem
{
    Q_OBJECT
public:
    explicit NodeViewDeclarativeItem(QDeclarativeItem *parent = 0);
    ~NodeViewDeclarativeItem();

    /*! The node this widget displays.
     * \return The GeglNode
     */
    GeglNodePtr inputNode();

    /*! Set the node to display
     * \param node The GeglNode to display, or 0 to unset
     *
     * Will reference the GeglNode.
     */
    void setInputNode(GeglNodePtr node);

    /*! The options used on this widget. */
    GeglQt::NodeViewOptions *options() const;

    //! \internal
    void setOptions(GeglQt::NodeViewOptions* newOptions);

    Q_PROPERTY(GeglQt::NodeViewOptions * options READ options WRITE setOptions NOTIFY optionsChanged)

    // QVariant is used so that GeglNode* can be passed through QML
    //! \internal
    void setInputNodeVariant(QVariant node);
    //! \internal
    QVariant inputNodeVariant();

    Q_PROPERTY(QVariant inputNode READ inputNodeVariant WRITE setInputNodeVariant NOTIFY inputNodeChanged)

public:
    //! \internal
    void geometryChanged(const QRectF & newGeometry, const QRectF & oldGeometry);

    //! \internal
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

Q_SIGNALS:
    void inputNodeChanged();
    void optionsChanged();

private Q_SLOTS:
    void invalidate(QRectF rect);
    void viewportSizeChangeRequested(QSizeF);

private:
    Q_DISABLE_COPY(NodeViewDeclarativeItem)
    NodeViewImplementation *priv;
};

}
#endif // GEGLQT_NODEVIEWDECLARATIVEITEM_H
