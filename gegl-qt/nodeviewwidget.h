#ifndef GEGLQT_NODEVIEWWIDGET_H
#define GEGLQT_NODEVIEWWIDGET_H

/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include <QWidget>
#include <gegl.h>

#include <gegl-qt/nodeviewoptions.h>

typedef GeglNode * GeglNodePtr;

namespace GeglQt {

class NodeViewImplementation;

/*!
 * QWidget showing the output of a GeglNode
 */

class NodeViewWidget : public QWidget
{
    Q_OBJECT

public:
    NodeViewWidget(QWidget *parent = 0);
    ~NodeViewWidget();

    /*! The node this widget displays.
     * \return The GeglNode
     */
    GeglNodePtr inputNode() const;

    /*! Set the node to display
     * \param node The GeglNode to display, or 0 to unset
     *
     * Will reference the GeglNode.
     */
    void setInputNode(GeglNodePtr node);

    /*! The options used on this widget. */
    GeglQt::NodeViewOptions *options() const;

public:
    //! \internal
    virtual void paintEvent(QPaintEvent *event);

    //! \internal
    virtual void resizeEvent(QResizeEvent *event);

private Q_SLOTS:
    void invalidate(QRectF rect);
    void viewportSizeChangeRequested(QSizeF);

private:
    Q_DISABLE_COPY(NodeViewWidget)
    NodeViewImplementation *priv;
};

}
#endif // GEGLQT_NODEVIEWWIDGET_H
