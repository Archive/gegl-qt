#ifndef GEGLQT_H
#define GEGLQT_H

/*!
 * \mainpage
 *
 * gegl-qt is an integration library for using GEGL in Qt-based applications.
 *
 * All recent versions of Qt4, possibly as far down as 4.2, are supported,
 * and all the different widget-paradims; QWidget, QGraphicsView and Qt Quick.
 *
 * gegl-qt can be used directly from C++, or from QML when build with Qt Quick,
 * or from Python when built with PySide. For usage examples, see
 * http://git.gnome.org/browse/gegl-qt/tree/examples
 *
 * gegl-qt is licensed under the GNU LGPL v3+ and is maintained by Jon Nordby
 *
 * - Homepage: http://www.gegl.org/gegl-qt
 * - Bugtracker: http://bugzilla.gnome.org, product: GEGL, component: gegl-qt
 * - Code: http://git.gnome.org/browse/gegl-qt/
 * - Mailinglist: http://mail.gnome.org/mailman/listinfo/gegl-developer-list
 *
 * For information on how to build, see the README.txt file in the source code.
 *
 * \section Widgets
 * gegl-qt currently provides one type of widget, a NodeView. It shows the output of a GeglNode.
 *
 * - For QGraphicsView: GeglQt::NodeViewGraphicsWidget
 * - For Qt Quick: GeglQt::NodeViewDeclarativeItem
 * - For QWidget: GeglQt::NodeViewWidget
 */

/*!
 * \namespace GeglQt
 *
 * \brief Main namespace for gegl-qt
 *
 */

// Node Views
#include <gegl-qt/nodeviewoptions.h>

// Code requiring QtWidgets
// FIXME: don't include when not build with widgets
#include <gegl-qt/nodeviewwidget.h>
#include <gegl-qt/nodeviewgraphicswidget.h>

// Code requiring QtDeclarative is found in the
// gegl-qt-declarative.h header

#endif // GEGLQT_H
