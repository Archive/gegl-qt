/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "nodeviewdeclarativeitem.h"
#include "internal/nodeviewimplementation.h"

using namespace GeglQt;

NodeViewDeclarativeItem::NodeViewDeclarativeItem(QDeclarativeItem *parent)
    : QDeclarativeItem(parent)
    , priv(new NodeViewImplementation(NodeViewImplementation::DrawModeChildGraphicsItem))
{
    setFlag(QGraphicsItem::ItemHasNoContents, false);

    priv->childItem()->setParentItem(static_cast<QGraphicsItem *>(this));

    connect(priv, SIGNAL(viewportSizeRequest(QSizeF)),
            this, SLOT(viewportSizeChangeRequested(QSizeF)));
}

NodeViewDeclarativeItem::~NodeViewDeclarativeItem()
{
    delete priv;
}

void
NodeViewDeclarativeItem::setInputNode(GeglNode *node)
{
    if (node != inputNode()) {
        priv->setInputNode(node);
        Q_EMIT inputNodeChanged();
    }
}

GeglNode *
NodeViewDeclarativeItem::inputNode()
{
    return priv->inputNode();
}

void
NodeViewDeclarativeItem::setInputNodeVariant(QVariant node)
{
    if (node.isValid()) {
        GeglNode *gegl_node = static_cast<GeglNode*>(node.value<void*>());
        setInputNode(gegl_node);
    }
}

QVariant
NodeViewDeclarativeItem::inputNodeVariant()
{
    return qVariantFromValue(static_cast<void*>(inputNode()));
}

void
NodeViewDeclarativeItem::setOptions(GeglQt::NodeViewOptions* newOptions)
{
    if (newOptions != options()) {
        priv->setOptions(newOptions);
        Q_EMIT optionsChanged();
    }
}

NodeViewOptions *
NodeViewDeclarativeItem::options() const
{
    return priv->options();
}

void
NodeViewDeclarativeItem::invalidate(QRectF rect)
{
    // Handled by private class
    Q_UNUSED(rect);
}

void
NodeViewDeclarativeItem::viewportSizeChangeRequested(QSizeF requestedSize)
{
    // XXX: Is this the correct way to do it?
    setImplicitHeight(requestedSize.height());
    setImplicitWidth(requestedSize.width());
}

void
NodeViewDeclarativeItem::paint(QPainter *painter,
                             const QStyleOptionGraphicsItem *option,
                             QWidget *widget)
{
    // Handled by private class
    Q_UNUSED(painter);
    Q_UNUSED(option);
    Q_UNUSED(widget);
}

void
NodeViewDeclarativeItem::geometryChanged(const QRectF & newGeometry,
                                       const QRectF & oldGeometry)
{
    QDeclarativeItem::geometryChanged(newGeometry, oldGeometry);

    priv->viewportSizeChanged(newGeometry.size());
}

