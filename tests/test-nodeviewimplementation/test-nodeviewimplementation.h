#ifndef TESTNODEVIEWIMPLEMENTATION_H
#define TESTNODEVIEWIMPLEMENTATION_H

#include <QObject>
#include <gegl.h>

#include <gegl-qt/internal/nodeviewimplementation.h>

class TestNodeViewImplementation : public QObject
{
    Q_OBJECT
public:
    explicit TestNodeViewImplementation(QObject *parent = 0);

private Q_SLOTS:
    void init();
    void clean();
    void initTestCase();
    void cleanupTestCase();

    void testSanity();

    void testComputedSignalEmitsViewAreaChanged_data();
    void testComputedSignalEmitsViewAreaChanged();

    void testViewAreaChangedWithTranslation_data();
    void testViewAreaChangedWithTranslation();

private:
    GeglQt::NodeViewImplementation *mViewImplementation;
    GeglNode *mGeglNode;
};

#endif // TESTNODEVIEWIMPLEMENTATION_H
