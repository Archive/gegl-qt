/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#include "test-nodeviewimplementation.h"

#include <QtTest>

using namespace GeglQt;

/* Tests the node view implementation, which is used by the different node view widgets.
 * TODO:
 * - Test that the GeglNode is processed (emits 'computed' signal) when invalidated
 * - Test scaling
 * - Test redraw behavior on option changes
 * - Test autoscaling
 * - Test autocentering
 */
TestNodeViewImplementation::TestNodeViewImplementation(QObject *parent) :
    QObject(parent)
{}

void TestNodeViewImplementation::initTestCase()
{
    gegl_init(NULL, NULL);
}

void TestNodeViewImplementation::cleanupTestCase()
{
    gegl_exit();
}

void TestNodeViewImplementation::init()
{
    mViewImplementation = new NodeViewImplementation();
    mGeglNode = gegl_node_new();

    mViewImplementation->setInputNode(mGeglNode);
}

void TestNodeViewImplementation::clean()
{
    g_object_unref(mGeglNode);
    delete mViewImplementation;
}

/* Does nothing. */
void TestNodeViewImplementation::testSanity()
{

}

/* When the attached GeglNode emits the "computed" signal,
 * the NodeViewImplementation shall emit a "viewAreaChanged" signal
 */
void TestNodeViewImplementation::testComputedSignalEmitsViewAreaChanged_data()
{
    QTest::addColumn<QRect>("input");
    QTest::addColumn<QRectF>("expectedOutput");

    QTest::newRow("0,0 100x100")
            << QRect(0, 0, 100, 100)
            << QRectF(0, 0, 100, 100);
    QTest::newRow("0,0 100000x100000")
            << QRect(0, 0, 100000, 100000)
            << QRectF(0, 0, 100000, 100000);

    QTest::newRow("13,13 1000x1000")
            << QRect(13, 13, 1000, 1000)
            << QRectF(13, 13, 1000, 1000);
    QTest::newRow("-13,-13 1000x1000")
            << QRect(-13, -13, 1000, 1000)
            << QRectF(-13, -13, 1000, 1000);

    QTest::newRow("130000,130000 1000x1000")
            << QRect(130000, 130000, 1000, 1000)
            << QRectF(130000, 130000, 1000, 1000);
    QTest::newRow("-130000,-130000 1000x1000")
            << QRect(-130000, -130000, 1000, 1000)
            << QRectF(-130000, -130000, 1000, 1000);

}
void TestNodeViewImplementation::testComputedSignalEmitsViewAreaChanged()
{
    /* NOTE: tests the default transformation as a side-effect */
    QFETCH(QRect, input);
    QFETCH(QRectF, expectedOutput);
    GeglRectangle computedRect = {input.x(), input.y(), input.width(), input.height()};

    QSignalSpy signalSpy(mViewImplementation, SIGNAL(viewAreaChanged(QRectF)));

    g_signal_emit_by_name(mGeglNode, "computed", &computedRect, NULL);

    QCOMPARE(signalSpy.count(), 1);
    QCOMPARE(signalSpy.at(0).first().toRectF(), expectedOutput);
}

/* Test that the computed -> viewAreaChanged obeys translations correctly */
void TestNodeViewImplementation::testViewAreaChangedWithTranslation_data()
{
    QTest::addColumn<QRect>("input");
    QTest::addColumn<QPointF>("translation");
    QTest::addColumn<QRectF>("expectedOutput");

    QTest::newRow("0,0 100x100. Translated by (0,0)")
            << QRect(0, 0, 100, 100) << QPointF(0,0)
            << QRectF(0, 0, 100, 100);
    QTest::newRow("0,0 100x100. Translated by (9,9)")
            << QRect(0, 0, 100, 100) << QPointF(9,9)
            << QRectF(9, 9, 100, 100);

    QTest::newRow("9,9 100x100. Translated by (9,9)")
            << QRect(9, 9, 100, 100) << QPointF(9,9)
            << QRectF(18, 18, 100, 100);
    QTest::newRow("-9,-9 100x100. Translated by (9,9)")
            << QRect(-9, -9, 100, 100) << QPointF(9,9)
            << QRectF(0, 0, 100, 100);

    QTest::newRow("9999,9999 10000x10000. Translated by (99,0)")
            << QRect(9999, 9999, 10000, 10000) << QPointF(99,0)
            << QRectF(10098, 9999, 10000, 10000);
    QTest::newRow("-9999,-9999 10000x10000. Translated by (0,99)")
            << QRect(-9999, -9999, 10000, 10000) << QPointF(0,99)
            << QRectF(-9999, -9900, 10000, 10000);

    QTest::newRow("100,100 100x100. Translated by (0.5,-0.5)")
            << QRect(100, 100, 100, 100) << QPointF(0.5,-0.5)
            << QRectF(100.5, 99.5, 100, 100);
    QTest::newRow("-100,-100 100x100. Translated by (-0.5,0.5)")
            << QRect(-100, -100, 100, 100) << QPointF(-0.5,0.5)
            << QRectF(-100.5, -99.5, 100, 100);

}
void TestNodeViewImplementation::testViewAreaChangedWithTranslation()
{
    /* NOTE: tests the default scale value as a side-effect */
    QFETCH(QRect, input);
    QFETCH(QPointF, translation);
    QFETCH(QRectF, expectedOutput);
    GeglRectangle computedRect = {input.x(), input.y(), input.width(), input.height()};

    mViewImplementation->options()->setTranslationX(translation.x());
    mViewImplementation->options()->setTranslationY(translation.y());

    QSignalSpy signalSpy(mViewImplementation, SIGNAL(viewAreaChanged(QRectF)));
    g_signal_emit_by_name(mGeglNode, "computed", &computedRect, NULL);

    QCOMPARE(signalSpy.count(), 1);
    QCOMPARE(signalSpy.at(0).first().toRectF(), expectedOutput);
}

QTEST_MAIN(TestNodeViewImplementation)

