isEmpty(GEGLQT_CONFIG):error("config.pri not included")
# This file uses variables from config.pri, so users must include config.pri before including this file

QT += testlib core
QT -= gui # Not all tests needs UI
CONFIG += debug

CONFIG += link_pkgconfig
PKGCONFIG += $$GEGL_PKG

isEmpty(USE_EXTERNAL_GEGLQT) {
    INCLUDEPATH += ../../gegl-qt ../.. # ../.. because public includes have gegl-qt/ prefix
    LIBS += -l$$GEGLQT_LIBNAME -L$$OUT_PWD/../../gegl-qt
    TEST_ENVIRONMENT = LD_LIBRARY_PATH=$$OUT_PWD/../../gegl-qt:$(LD_LIBRARY_PATH)
} else {
    CONFIG += link_pkgconfig
    PKGCONFIG += $$GEGLQT_LIBNAME
    INSTALLED_GEGLQT_LIBDIR=$$system(pkg-config $$GEGLQT_LIBNAME --variable=libdir)
    TEST_ENVIRONMENT = LD_LIBRARY_PATH=$$INSTALLED_GEGLQT_LIBDIR:$(LD_LIBRARY_PATH)
}

QMAKE_EXTRA_TARGETS += check
check.target = check
check.commands =  $$TEST_ENVIRONMENT ./$$TARGET
check.depends += $$TARGET

OBJECTS_DIR = .obj
MOC_DIR = .moc
