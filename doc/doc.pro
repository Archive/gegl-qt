include(../config.pri)
isEmpty(GEGLQT_CONFIG):error("config.pri not found")

TEMPLATE = subdirs

outputFiles(Doxyfile)
OTHER_FILES += DoxyFile.in


contains(HAVE_DOXYGEN, yes) {
    QMAKE_EXTRA_TARGETS += first
    first.depends += doc

    contains(HAVE_PDFLATEX, yes) {
        first.depends += pdfdoc
    }
}

QMAKE_EXTRA_TARGETS += doc
doc.target = doc
doc.commands += doxygen Doxyfile

QMAKE_EXTRA_TARGETS += pdfdoc
pdfdoc.target = pdfdoc
pdfdoc.depends = doc
pdfdoc.commands += cd latex && make
