gegl-qt: Qt utility library for GEGL.

License: LGPLv3+
Maintainer: Jon Nordby <jononor@gmail.com>
Homepage: http://www.gegl.org/gegl-qt
Code: http://git.gnome.org/browse/gegl-qt/
Bugtracker: http://bugs.gnome.org, product GEGL (use gegl-qt in title)
Mailinglist: http://mail.gnome.org/mailman/listinfo/gegl-developer-list

== Installing ==
gegl-qt uses the qmake build system. To install do:
 qmake -r
 make
 make install

To install to a custom prefix, pass GEGLQT_INSTALL_PREFIX to qmake. Example:
 qmake -r GEGLQT_INSTALL_PREFIX=/home/user/gegl-qt

gegl-qt uses pkg-config to find its dependencies, set PKG_CONFIG_PATH to
configure which dependencies it is built against.

== Using ==
To use gegl-qt in your project, use the provided pkg-config file.
 pkg-config gegl-qt4-0.1 --libs --cflags

== Documentation ==
See this file, the examples and source code, for now. :)

== Features ==

- Support for both Qt4 and Qt5.

=== View widgets ===
- Shows the output of a Gegl graph.
- QML, QGraphicsView and QWidget versions are provided.
- Scaling and translation is supported.
- Can do automatic scaling and centering

=== Gegl display operation ===
Allows applications to show a GEGL graph in a window without linking
against a specific UI library.

== Purpose & Scope ==
The purpose of gegl-qt is to make it easy for application developers to
use GEGL in applications with Qt based user interfaces.

gegl-qt aims to support Qt4 and Qt5 for both QWidget, QGraphicsView and QML based user interfaces.

== Contributing ==
To contribute code, please file a bug and attach git-formatted patches there, or link to
a public git branch which has the commits (on github for instance).
For quick, simple and obvious fixes, you may send a patch to the mailinglist.

For a list of tasks that needs to be done see the TODO section of this document,
and the TODO, FIXME and XXX comments in the source code.

== TODO ==
Currently the focus is on the 0.1.0 release. The 0.1 series will
have a basic set of features, and maintain ABI and API compatability.

=== 0.1.0 ===
First release. Goal: minimally useful, solid interfaces.
Should at happen before or together with the next Gegl release.
Need to build and work with whatever is the latest Gegl release.

- Do API review

- Complete Qt5 support

- Have a basic set of tests, and good test infrastructure
- Distribute a set of basic examples for the provided features
 * Missing: use of view->model transformations
 * Examples needs to have a description, and be commented

=== Later ===

Display operation
- Display operation needs to spin its own mainloop
- Register the display operation as a 'display' type operation (needs support in Gegl)

View widgets
- Rotation support
- Background and overlay draw support for GeglQt::NodeView

QImage IO loader plugin that uses GEGL loaders

QML Gegl bindings
- Use GObject Introspection + Smoke GObject to provide Qt bindings,
so that Gegl can be used in pure QML

Tests
- Unittests for view widgets
- Regression tests for view widgets rendering
- Performance benchmarks for view widgets

Performance
- See OPTIMIZE comments in the code

Examples
- Interactive examples of Gegl + GeglQt features

=== Maybe ===
Additional widgets?

- Graph view: Show the entire Gegl graph and allow to manipulate it
Add/remove and reorder operations.
- Node properties view: show the properties of a given Gegl node in a graph,
and allow to manipulate them.

