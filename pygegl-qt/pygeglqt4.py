
import sys, os.path

# Used so that we can support install of multiple parallel
#
def require(version):
    """Set up the import for a given gegl-qt version.

    After calling this function the module 'geglqt' can be imported and used.
    Will raise ValueError on unsupported

    Example:
    import pygeglqt4
    pygeglqt4.require("0.1")
    from geglqt import GeglQt
    """

    if version != "0.1":
        raise ValueError, "Unsupported version or invalid version argument: %s" % (repr(version),)

    # The module is installed in a versioned directory relative to this file
    # Locate it and prepend it to the python import path
    base_dir = os.path.abspath(os.path.dirname(__file__))
    module_dir = 'gegl-qt4-%s' % (version,)
    module_path = os.path.join(base_dir, module_dir)
    sys.path.insert(0, module_path)
