include(../config.pri)

TEMPLATE = lib

QMAKE_EXTRA_TARGETS += first
first.depends += generate compile link

TYPESYSTEM_DIR = $$system(pkg-config pyside --variable=typesystemdir)

LIBGEGLQT_SRC_DIR = $$IN_PWD/../gegl-qt
LIBGEGLQT_BUILD_DIR = $$OUT_PWD/../gegl-qt
TOP_SRC_DIR = $$IN_PWD/..
TOP_BUILD_DIR = $$OUT_PWD/..
PYSIDE_INCLUDE_DIR = $$system(pkg-config --variable=includedir pyside)
HEADERDIR_QT = $$[QT_INSTALL_HEADERS]
GEN_INCLUDE_PATHS = $$TOP_SRC_DIR:$$LIBGEGLQT_SRC_DIR:$$HEADERDIR_QT/QtCore:$$HEADERDIR_QT

INCLUDES += \
    -I$$TOP_BUILD_DIR \
    -I$$TOP_SRC_DIR \
    -I$$LIBGEGLQT_BUILD_DIR \
    -I$$LIBGEGLQT_SRC_DIR \
    -I$$PYSIDE_INCLUDE_DIR/QtCore \
    -I$$PYSIDE_INCLUDE_DIR/QtGui \
    -I$$[QT_INSTALL_HEADERS]/QtCore \
    -I$$[QT_INSTALL_HEADERS]/QtGui

contains(HAVE_QT_DECLARATIVE, yes) {
    INCLUDES += \
        -I$$PYSIDE_INCLUDE_DIR/QtDeclarative \
        -I$$PYSIDE_INCLUDE_DIR/QtNetwork \
        -I$$[QT_INSTALL_HEADERS]/QtDeclarative \
        -I$$[QT_INSTALL_HEADERS]/QtNetwork
}

INCLUDES += $$system(pkg-config --cflags gegl pygobject-2.0 gobject-2.0 pyside)

# pkg-config --libs pyside is buggy for pyside<1.0.5 due
# http://bugs.pyside.org/show_bug.cgi?id=929
# PYSIDE_LIBS = /usr/lib/libpython2.7.so -L/usr/lib -lpyside-python2.7 -lshiboken-python2.7

outputFiles(global.h typesystem_gegl-qt.xml)

OTHER_FILES += \
    global.h.in \
    typesystem_gegl-qt.xml.in \
    geglnode_conversions.h \
    pygeglqt4.py \

# Generate
QMAKE_EXTRA_TARGETS += generate
generate.target = generate
generate.commands += generatorrunner --generatorSet=shiboken \
                global.h \
                --include-paths=$$GEN_INCLUDE_PATHS \
                --typesystem-paths=$$OUT_PWD:$$IN_PWD:$$TYPESYSTEM_DIR \
                --output-directory=. \
                typesystem_gegl-qt.xml

# Compile
QMAKE_EXTRA_TARGETS += compile
compile.depends += generate
compile.target = compile
compile.commands += g++ -DNO_IMPORT_PYGOBJECT geglqt/geglqt_*.cpp $$INCLUDES -Wall -fPIC -c;
compile.commands += g++ geglqt/geglqt_module_wrapper.cpp $$INCLUDES -Wall -fPIC -c


LIBRARIES += -L../../gegl-qt -l$$GEGLQT_LIBNAME
LIBRARIES += $$system(pkg-config --libs gegl pygobject-2.0 gobject-2.0 pyside)

# Link
QMAKE_EXTRA_TARGETS += link
link.depends += compile
link.target = $$GEGLQT_LIBNAME/geglqt.so
link.commands += mkdir -p $$GEGLQT_LIBNAME;
link.commands += cd $$GEGLQT_LIBNAME;
link.commands += g++ ../geglqt*wrapper.o $$LIBRARIES -fPIC -shared -Wl,-soname,geglqt.so -o geglqt.so

# Install
PYTHON_SITE_PACKAGES = $$system(`echo $PYTHON` -c \"from distutils.sysconfig import get_python_lib; print get_python_lib(True)\")
isEmpty(PYTHON_SITE_PACKAGES) {
    PYTHON_SITE_PACKAGES = $$system(python -c \"from distutils.sysconfig import get_python_lib; print get_python_lib(True)\")

}

modules.files = $$GEGLQT_LIBNAME/geglqt.so
modules.CONFIG += no_check_exist
modules.path = $$PYTHON_SITE_PACKAGES/$$GEGLQT_LIBNAME

wrapper.files = pygeglqt4.py
wrapper.path = $$PYTHON_SITE_PACKAGES

INSTALLS += modules wrapper




