#ifndef QMLPLUGIN_H
#define QMLPLUGIN_H

#include <QDeclarativeExtensionPlugin>

class QmlPlugin : public QDeclarativeExtensionPlugin
{
    Q_OBJECT
public:
    explicit QmlPlugin(QObject *parent = 0);

    virtual void registerTypes(const char *uri);
};

#endif // QMLPLUGIN_H
