include(../../config.pri)

TEMPLATE = lib
CONFIG += qt plugin no_keywords

contains(HAVE_QT_WIDGETS, yes) {
    QT += $$QT_WIDGETS
}

contains(HAVE_QT_DECLARATIVE, yes) {
    QT += $$QT_DECLARATIVE
}

TARGET = $$GEGLQT_QML_API_NAME/$$GEGLQT_LIBNAME

target.path = $$QTDECLARATIVE_INSTALL_PLUGINS/$$GEGLQT_QML_API_NAME

HEADERS += qml-plugin.h
SOURCES += qml-plugin.cpp

INCLUDEPATH += ../../gegl-qt ../.. # .. because public include have gegl-qt/ prefix
LIBS += -L../../gegl-qt -l$$GEGLQT_LIBNAME

CONFIG += link_pkgconfig
PKGCONFIG += $$GEGL_PKG

OTHER_FILES += \
    $$GEGLQT_QML_API_NAME/qmldir.in

outputFiles($$GEGLQT_QML_API_NAME/qmldir)

qmldir_install.files += $$GEGLQT_QML_API_NAME/qmldir
qmldir_install.path += $$target.path

INSTALLS += target qmldir_install
