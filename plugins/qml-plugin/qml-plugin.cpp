#include "qml-plugin.h"

#include <gegl-qt.h>
#include <gegl-qt-declarative.h>

#include <QtDeclarative>

using namespace GeglQt;

QmlPlugin::QmlPlugin(QObject *parent) :
    QDeclarativeExtensionPlugin(parent)
{
}

void QmlPlugin::registerTypes(const char *uri)
{
    qmlRegisterType<NodeViewDeclarativeItem>(uri, 0, 1, "NodeView");
    qmlRegisterType<NodeViewOptions>(uri, 0, 1, "NodeViewOptions");
}

Q_EXPORT_PLUGIN2(qmlplugin, QmlPlugin)
