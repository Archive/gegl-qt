include(../config.pri)

TEMPLATE = subdirs

# Currently don't have anything to register for non-quick1 case, so no bother building
contains(HAVE_QT_QUICK1, yes) {
    SUBDIRS += qml-plugin
}
