include(../config.pri)

TARGET = $$GEGLQT_BASELIBNAME-display
target.path = $$GEGL_INSTALL_OPERATIONS
TEMPLATE = lib
CONFIG += qt no_keywords plugin

VERSION = $$GEGLQT_VERSION

QT += core gui
contains(HAVE_QT_WIDGETS, yes) {
    QT += $$QT_WIDGETS
}

GEGLQT_OPERATION_PREFIX = gegl-qt$$QT_MAJOR_VERSION

DEFINES += GEGLQT_OPERATION_PREFIX=\\\"$${GEGLQT_OPERATION_PREFIX}\\\"

CONFIG += link_pkgconfig
PKGCONFIG += $$GEGL_PKG

INCLUDEPATH += ../gegl-qt .. # .. because public include have gegl-qt/ prefix
LIBS += -L../gegl-qt -l$$GEGLQT_LIBNAME

SOURCES += \
    geglqtdisplay.cpp \

INSTALLS += target

