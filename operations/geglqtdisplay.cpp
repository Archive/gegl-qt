/* This file is part of GEGL-QT
 *
 * GEGL-QT is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * GEGL-QT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with GEGL-QT; if not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (C) 2011 Jon Nordby <jononor@gmail.com>
 */

#ifdef GEGL_CHANT_PROPERTIES

gegl_chant_string  (window_title, "Window Title", "", "Title to give window.")

#else

#define GEGL_CHANT_TYPE_SINK
#define GEGL_CHANT_C_FILE   "geglqtdisplay.cpp"

#include <gegl.h>
#include <gegl-chant.h>

#include <QtGui>
#include <gegl-qt.h>

using namespace GeglQt;

typedef struct
{
  NodeViewWidget *viewWidget;
  GeglNode *node;
} Priv;


static Priv *
init_priv (GeglOperation *operation)
{
  GeglChantO *o = GEGL_CHANT_PROPERTIES (operation);

  if (!o->chant_data)
    {
      Priv *priv = g_new0 (Priv, 1);
      o->chant_data = (void*) priv;

      priv->viewWidget = new NodeViewWidget();
      priv->viewWidget->options()->setAutoScalePolicy(NodeViewOptions::AutoScaleViewport);
      priv->viewWidget->show();
    }
  return (Priv*)(o->chant_data);
}


static void
attach (GeglOperation *operation)
{
  Priv       *priv = init_priv (operation);

  GeglNode *input = gegl_node_get_input_proxy (operation->node, "input");
  priv->node = gegl_node_new_child (operation->node,
                                    "operation", "gegl:nop",
                                    NULL);

  gegl_node_link (input, priv->node);
  priv->viewWidget->setInputNode(priv->node);
}

static void
dispose (GObject *object)
{
  GeglChantO *o = GEGL_CHANT_PROPERTIES (object);
  Priv       *priv = (Priv*)o->chant_data;

  if (priv)
    {
      // Destroy priv
      delete priv->viewWidget;
      priv->viewWidget = 0;

      g_object_unref(priv->node);

      o->chant_data = NULL;
    }

  G_OBJECT_CLASS (g_type_class_peek_parent (G_OBJECT_GET_CLASS (object)))->dispose (object);
}


static void
gegl_chant_class_init (GeglChantClass *klass)
{
  GeglOperationClass     *operation_class;
  GeglOperationSinkClass *sink_class;

  operation_class = GEGL_OPERATION_CLASS (klass);
  sink_class      = GEGL_OPERATION_SINK_CLASS (klass);

  operation_class->attach = attach;
  G_OBJECT_CLASS (klass)->dispose = dispose;

#if GEGL_MINOR_VERSION >= 2 && GEGL_MICRO_VERSION >= 0
  gegl_operation_class_set_keys (operation_class,
      "name", GEGLQT_OPERATION_PREFIX":display",
      "categories", "output",
      "description", "Displays the input buffer in a Qt window .",
      NULL);
#else /* GEGL < 0.2.0 */
  operation_class->name        = GEGLQT_OPERATION_PREFIX":display";
  operation_class->categories  = "output";
  operation_class->description = "Displays the input buffer in a Qt window.";
#endif
}

#endif
