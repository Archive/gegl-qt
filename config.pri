
GEGLQT_CONFIG = TRUE # Must always be set in this file. Used to detect if the file was found or not

GEGLQT_PROJECTNAME = gegl-qt
GEGLQT_VERSION = 0.0.7
GEGLQT_PROJECTBRIEF = "Integration library for using GEGL in Qt based applications"

GEGLQT_API_VERSION = 0.1
GEGLQT_BASELIBNAME = gegl-qt$$QT_MAJOR_VERSION
GEGLQT_LIBNAME = $$GEGLQT_BASELIBNAME-$$GEGLQT_API_VERSION

GEGLQT_QML_API_NAME = GeglQt$$QT_MAJOR_VERSION
GEGLQT_QML_API_VERSION_MAJOR = 0
GEGLQT_QML_API_VERSION_MINOR = 1
GEGLQT_QML_API_VERSION = 0.1

OBJECTS_DIR = .obj
MOC_DIR = .moc

### Hard dependencies ###

# GEGL
HAVE_GEGL = no
GEGL_PKG = ""

contains(HAVE_GEGL, no) {
    system(pkg-config --exists gegl-0.3) {
        HAVE_GEGL = 0.3
        GEGL_PKG = gegl-0.3
    }
}

contains(HAVE_GEGL, no) {
    system(pkg-config --exists gegl-0.2) {
        HAVE_GEGL = 0.2
        GEGL_PKG = gegl-0.2
    }
}

contains(HAVE_GEGL, no) {
    system(pkg-config --exists gegl) {
        HAVE_GEGL = 0.1
        GEGL_PKG = gegl
    }
}

contains(HAVE_GEGL, no):error("Could not find required dependency: GEGL")

### Options ###
# Can be used to specify custom install prefix
isEmpty(GEGLQT_INSTALL_PREFIX) {
    GEGLQT_INSTALL_PREFIX = /usr
}

!isEmpty(GEGLQT_INTEGRATION_CHECK) {
    # Build for integration check
    # Compile examples and tests against an installed gegl-qt version
    USE_EXTERNAL_GEGLQT = yes
}

# Paths for installing files
GEGL_LIBDIR = $$system(pkg-config --variable libdir $$GEGL_PKG)
GEGL_INSTALL_OPERATIONS = $$system(pkg-config --variable pluginsdir $$GEGL_PKG)
isEmpty(GEGL_INSTALL_OPERATIONS) {
    GEGL_INSTALL_OPERATIONS = $$GEGL_LIBDIR/gegl-$$HAVE_GEGL/
}

#
isEmpty(QTDECLARATIVE_INSTALL_PLUGINS) {
    QTDECLARATIVE_INSTALL_PLUGINS = $$[QT_INSTALL_IMPORTS]
}

### Optional deps ###
# QtDeclarative
HAVE_QT_DECLARATIVE = yes
contains(QT_MAJOR_VERSION, 5) {
    !contains(QT_CONFIG, declarative) {
        HAVE_QT_DECLARATIVE = no
    }
} else {
    !system(pkg-config QtDeclarative) {
        HAVE_QT_DECLARATIVE = no
    }
}

# QtQuick1 provides QDeclarativeView and QDeclarativeItem
# On Qt4 it is actually a part of the QtDeclarative module
HAVE_QT_QUICK1 = yes
contains(QT_MAJOR_VERSION, 5) {
    !contains(QT_CONFIG, quick1) {
        HAVE_QT_QUICK1 = no
    }
} else {
    !contains(HAVE_QT_DECLARATIVE, yes) {
        HAVE_QT_QUICK1 = no
    }
}

# On Qt5, quick or quick1 is required in addition to declarative for "QT" option
# On Qt4, it should only contain declarative
contains(QT_MAJOR_VERSION, 5) {
    QT_DECLARATIVE = declarative quick
    contains(HAVE_QT_QUICK1, yes) {
        QT_DECLARATIVE += quick1
    }
} else {
    QT_DECLARATIVE = declarative
}

contains(HAVE_QT_DECLARATIVE, no) {
    !build_pass:system(echo "QtDeclarative not found - no QML support")
}

isEmpty(HAVE_PYSIDE) {
    HAVE_PYSIDE = yes
    !system(pkg-config pyside){
        !build_pass:system(echo "PySide not found - no Python support")
        HAVE_PYSIDE = no
    }
}

# Can go away when PySide supports Qt5
contains(QT_MAJOR_VERSION, 5) {
    !build_pass:system(echo "PySide not supported with Qt 5 - no Python support")
    HAVE_PYSIDE = no
}

HAVE_DOXYGEN = yes
DOXYGEN = $$system(which doxygen)
isEmpty(DOXYGEN){
    !build_pass:system(echo "doxygen not found - no documentation")
    HAVE_DOXYGEN = no
}

HAVE_PDFLATEX = yes
PDFLATEX = $$system(which pdflatex)
isEmpty(PDFLATEX){
    !build_pass:system(echo "pdflatex not found - no PDF documentation")
    HAVE_PDFLATEX = no
}

# QtWidgets
HAVE_QT_WIDGETS = yes
contains(QT_MAJOR_VERSION, 5) {
    !system(pkg-config QtWidgets) {
        HAVE_QT_WIDGETS = no
    }
} else {
    # Qt4 always has QtWidgets
}

!contains(HAVE_QT_WIDGETS, yes) {
    !build_pass:system(echo "QtWidgets not found - no QtWidgets support")
}

# On Qt5, widgets is required in addition to gui for "QT" option
# On Qt4, it should be empty
contains(QT_MAJOR_VERSION, 5) {
    QT_WIDGETS = widgets
}

GEGLQT_INSTALL_BIN = $$GEGLQT_INSTALL_PREFIX/bin
GEGLQT_INSTALL_HEADERS = $$GEGLQT_INSTALL_PREFIX/include
GEGLQT_INSTALL_LIBS = $$GEGLQT_INSTALL_PREFIX/lib
GEGLQT_INSTALL_DATA = $$GEGLQT_INSTALL_PREFIX/share

contains(HAVE_QT_DECLARATIVE, yes) {
    # Need to be conditionally included in python binding generation
    GEGLQT_DECLARATIVE_INCLUDE = "$$LITERAL_HASH\\include <GeglQtDeclarative>"
    GEGLQT_DECLARATIVE_TYPESYSTEM = "<object-type name=\\\"NodeViewDeclarativeItem\\\"/>"
    GEGLQT_DECLARATIVE_PKGCONFIG = "QtDeclarative"
}

# Variables that can be substituted in .in files
SUBST_VARIABLES += \
    GEGLQT_API_VERSION \
    GEGLQT_VERSION \
    GEGLQT_BASELIBNAME \
    GEGLQT_LIBNAME \
    GEGLQT_INSTALL_PREFIX \
    GEGLQT_INSTALL_BIN \
    GEGLQT_INSTALL_HEADERS \
    GEGLQT_INSTALL_LIBS \
    GEGLQT_INSTALL_DATA \
    GEGLQT_DECLARATIVE_INCLUDE \
    GEGLQT_DECLARATIVE_TYPESYSTEM \
    GEGLQT_DECLARATIVE_PKGCONFIG \
    GEGLQT_PROJECTBRIEF \
    GEGLQT_PROJECTNAME \
    GEGLQT_IN_PWD \
    GEGL_PKG \

# Generate the specified file from its .in template, and substitute variables
# Variables to be substituted in .in files must be of the form @VARIABLE_NAME@
defineTest(outputFile) {
    out = $$OUT_PWD/$$1
    in = $$PWD/$${1}.in

    GEGLQT_IN_PWD = $$IN_PWD

    !exists($$in) {
        error($$in does not exist!)
        return(false)
    }

    variables = $$SUBST_VARIABLES

    command = "sed"
    for(var, variables) {
       command += "-e \"s:@$$var@:$$eval($$var):g\""
    }
    command += $$in > $$out

    system(mkdir -p $$dirname(out))
    system($$command)
    system(chmod --reference=$$in $$out)

    return(true)
}

# Same as outputFile, just for multiple files
defineTest(outputFiles) {
    files = $$ARGS

    for(file, files) {
        !outputFile($$file):return(false)
    }

    return(true)
}
